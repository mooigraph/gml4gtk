class sugi2 extends c2jrt
{
   private static String class_name = "sugi2";


   static Class pcsetbit[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int csetbit;
   static Class pcclearbit[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cclearbit;
   static Class pctestbit[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int ctestbit;
   static Class pcmget[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cmget;
   static Class pcmget_set[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cmget_set;
   static Class pcnumber_of_crossings2[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cnumber_of_crossings2;
   static Class pcnumber_of_crossings3[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cnumber_of_crossings3;
   static Class pcnumber_of_crossings_a[]=
   {
   java.lang.Integer.TYPE
   }
   ;
   public static int cnumber_of_crossings_a;
   static Class pcmake_matrix[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cmake_matrix;
   static Class pcsu_find_node_with_number[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int csu_find_node_with_number;
   static Class pcstore_new_positions[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cstore_new_positions;
   static Class pccopy_m[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int ccopy_m;
   static Class pcequal_m[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cequal_m;
   static Class pcequal_a[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cequal_a;
   static Class pccopy_a[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int ccopy_a;
   static Class pcexch_rows[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cexch_rows;
   static Class pcexch_columns[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cexch_columns;
   static Class pcreverse_r[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int creverse_r;
   static Class pcreverse_c[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int creverse_c;
   static Class pcrow_barycenter[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int crow_barycenter;
   static Class pccolumn_barycenter[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int ccolumn_barycenter;
   static Class pcr_r[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cr_r;
   static Class pcr_c[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cr_c;
   static Class pcb_r[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cb_r;
   static Class pcb_c[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cb_c;
   static Class pcsorted[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int csorted;
   static Class pcphase1_down[]=
   {
   java.lang.Integer.TYPE
   }
   ;
   public static int cphase1_down;
   static Class pcphase1_up[]=
   {
   java.lang.Integer.TYPE
   }
   ;
   public static int cphase1_up;
   static Class pcphase2_down[]=
   {
   java.lang.Integer.TYPE
   }
   ;
   public static int cphase2_down;
   static Class pcphase2_up[]=
   {
   java.lang.Integer.TYPE
   }
   ;
   public static int cphase2_up;
   static Class pcbc_n[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int cbc_n;
   static Class pcreduce_crossings2[]=
   {
   java.lang.Integer.TYPE,java.lang.Integer.TYPE,java.lang.Integer.TYPE
   }
   ;
   public static int creduce_crossings2;

   static
   {
    try
   {
      csetbit=getMethod(Class.forName(class_name),"csetbit",pcsetbit);
      cclearbit=getMethod(Class.forName(class_name),"cclearbit",pcclearbit);
      ctestbit=getMethod(Class.forName(class_name),"ctestbit",pctestbit);
      cmget=getMethod(Class.forName(class_name),"cmget",pcmget);
      cmget_set=getMethod(Class.forName(class_name),"cmget_set",pcmget_set);
      cnumber_of_crossings2=getMethod(Class.forName(class_name),"cnumber_of_crossings2",pcnumber_of_crossings2);
      cnumber_of_crossings3=getMethod(Class.forName(class_name),"cnumber_of_crossings3",pcnumber_of_crossings3);
      cnumber_of_crossings_a=getMethod(Class.forName(class_name),"cnumber_of_crossings_a",pcnumber_of_crossings_a);
      cmake_matrix=getMethod(Class.forName(class_name),"cmake_matrix",pcmake_matrix);
      csu_find_node_with_number=getMethod(Class.forName(class_name),"csu_find_node_with_number",pcsu_find_node_with_number);
      cstore_new_positions=getMethod(Class.forName(class_name),"cstore_new_positions",pcstore_new_positions);
      ccopy_m=getMethod(Class.forName(class_name),"ccopy_m",pccopy_m);
      cequal_m=getMethod(Class.forName(class_name),"cequal_m",pcequal_m);
      cequal_a=getMethod(Class.forName(class_name),"cequal_a",pcequal_a);
      ccopy_a=getMethod(Class.forName(class_name),"ccopy_a",pccopy_a);
      cexch_rows=getMethod(Class.forName(class_name),"cexch_rows",pcexch_rows);
      cexch_columns=getMethod(Class.forName(class_name),"cexch_columns",pcexch_columns);
      creverse_r=getMethod(Class.forName(class_name),"creverse_r",pcreverse_r);
      creverse_c=getMethod(Class.forName(class_name),"creverse_c",pcreverse_c);
      crow_barycenter=getMethod(Class.forName(class_name),"crow_barycenter",pcrow_barycenter);
      ccolumn_barycenter=getMethod(Class.forName(class_name),"ccolumn_barycenter",pccolumn_barycenter);
      cr_r=getMethod(Class.forName(class_name),"cr_r",pcr_r);
      cr_c=getMethod(Class.forName(class_name),"cr_c",pcr_c);
      cb_r=getMethod(Class.forName(class_name),"cb_r",pcb_r);
      cb_c=getMethod(Class.forName(class_name),"cb_c",pcb_c);
      csorted=getMethod(Class.forName(class_name),"csorted",pcsorted);
      cphase1_down=getMethod(Class.forName(class_name),"cphase1_down",pcphase1_down);
      cphase1_up=getMethod(Class.forName(class_name),"cphase1_up",pcphase1_up);
      cphase2_down=getMethod(Class.forName(class_name),"cphase2_down",pcphase2_down);
      cphase2_up=getMethod(Class.forName(class_name),"cphase2_up",pcphase2_up);
      cbc_n=getMethod(Class.forName(class_name),"cbc_n",pcbc_n);
      creduce_crossings2=getMethod(Class.forName(class_name),"creduce_crossings2",pcreduce_crossings2);
   }catch(Exception ex){
    ex.printStackTrace();
   }
   }
static int strsugi22= jtocstr("setbit(0)\n");
   
   /* file sugi2.c line 63*/
   
   public static void csetbit(int  ca,int  ck)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int y1= 0;
         
         {
         
         /* file sugi2.c line 65*/
         if( (((ck)==(0))?1:0)!=0)
         {
         	   
            {
            
            /* file sugi2.c line 66*/
            cprintf(strsugi22,0);
            
            /* file sugi2.c line 67*/
            cfflush((int)((c_streamv + ((int)(1)*48))));
            
            }
         
         }
         else
         {
         	   
            {
            y1= (int)((ca + ((int)((ck/8))*1)));
            setMEMUBYTE((int)(y1),(((getMEMUBYTE((int)(y1))&0xff) | ((char)((1<<(ck%8)))&0xff))));
            
            }
         
         };
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
static int strsugi23= jtocstr("clearbit(0)\n");
   
   /* file sugi2.c line 78*/
   
   public static void cclearbit(int  ca,int  ck)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int y2= 0;
         
         {
         
         /* file sugi2.c line 80*/
         if( (((ck)==(0))?1:0)!=0)
         {
         	   
            {
            
            /* file sugi2.c line 81*/
            cprintf(strsugi23,0);
            
            /* file sugi2.c line 82*/
            cfflush((int)((c_streamv + ((int)(1)*48))));
            
            }
         
         }
         else
         {
         	   
            {
            y2= (int)((ca + ((int)((ck/8))*1)));
            setMEMUBYTE((int)(y2),(((getMEMUBYTE((int)(y2))&0xff) & ((char)((~(1<<(ck%8))))&0xff))));
            
            }
         
         };
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
static int strsugi24= jtocstr("testbit(0)\n");
   
   /* file sugi2.c line 95*/
   
   public static int ctestbit(int  ca,int  ck)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
         
         {
         
         /* file sugi2.c line 97*/
         if( (((ck)==(0))?1:0)!=0)
         {
         	   
            {
            
            /* file sugi2.c line 98*/
            cprintf(strsugi24,0);
            
            /* file sugi2.c line 99*/
            cfflush((int)((c_streamv + ((int)(1)*48))));
            
            }
         
         };
         retval= (((((getMEMUBYTE((int)((ca + ((int)((ck/8))*1))))&0xff) & (1<<(ck%8))))!=(0))?1:0);
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 106*/
   
   public static int cmget(int  cm,int  ci,int  cj)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
         
         {
         retval= 
         /* file sugi2.c line 107*/
         ctestbit((int)(getMEMINT((int)((cm + 40)))),((ci*(getMEMINT((int)((cm + 8))) + 0)) + cj));
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 112*/
   
   public static void cmget_set(int  cm,int  ci,int  cj,int  cvalue)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
         
         {
         
         /* file sugi2.c line 114*/
         if( cvalue!=0)
         {
         	   
            {
            
            /* file sugi2.c line 115*/
            csetbit((int)(getMEMINT((int)((cm + 40)))),((ci*(getMEMINT((int)((cm + 8))) + 0)) + cj));
            
            }
         
         }
         else
         {
         	   
            {
            
            /* file sugi2.c line 119*/
            cclearbit((int)(getMEMINT((int)((cm + 40)))),((ci*(getMEMINT((int)((cm + 8))) + 0)) + cj));
            
            }
         
         };
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 128*/
   
   public static int cnumber_of_crossings2(int  cm,int  cr,int  cc)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  cj_6= 0;
      int  ck_7= 0;
      int  calpha_8= 0;
      int  cbeta_9= 0;
      int  cresult_10= 0;
         
         {
         cj_6= 0;
         ck_7= 0;
         calpha_8= 0;
         cbeta_9= 1;
         cresult_10= 0;
         
         /* file sugi2.c line 149*/
         cj_6=1;
         lab_sugi20 : for( ; (((cj_6)<=((cr - 1)))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 148*/
            ck_7=(cj_6 + 1);
            lab_sugi21 : for( ; (((ck_7)<=(cr))?1:0)!=0 ; )
            {
               
               {
               
               /* file sugi2.c line 147*/
               calpha_8=1;
               lab_sugi22 : for( ; (((calpha_8)<=((cc - 1)))?1:0)!=0 ; )
               {
                  
                  {
                  
                  /* file sugi2.c line 146*/
                  cbeta_9=(calpha_8 + 1);
                  lab_sugi23 : for( ; (((cbeta_9)<=(cc))?1:0)!=0 ; )
                  {
                     
                     {
                     cresult_10=(cresult_10 + (
                     /* file sugi2.c line 145*/
                     cmget(cm,cj_6,cbeta_9)*
                     /* file sugi2.c line 145*/
                     cmget(cm,ck_7,calpha_8)));
                     
                     }
                  cbeta_9++;
                  
                  }
                  
                  }
               calpha_8++;
               
               }
               
               }
            ck_7++;
            
            }
            
            }
         cj_6++;
         
         }
         retval= cresult_10;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 156*/
   
   public static int cnumber_of_crossings3(int  cm,int  cr,int  cc)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  cj_6= 0;
      int  ck_7= 0;
      int  calpha_8= 0;
      int  cbeta_9= 0;
      int  cresult2_10= 0;
         
         {
         cj_6= 0;
         ck_7= 0;
         calpha_8= 0;
         cbeta_9= 1;
         cresult2_10= 0;
         
         /* file sugi2.c line 164*/
         if( 0!=0)
         {
         	   
            {
            cresult2_10=
            /* file sugi2.c line 165*/
            cnumber_of_crossings2(cm,cr,cc);
            
            }
         
         };
         
         /* file sugi2.c line 191*/
         cj_6=1;
         lab_sugi24 : for( ; (((cj_6)<=((cr - 1)))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 190*/
            ck_7=(cj_6 + 1);
            lab_sugi25 : for( ; (((ck_7)<=(cr))?1:0)!=0 ; )
            {
               
               {
               
               /* file sugi2.c line 189*/
               calpha_8=1;
               lab_sugi26 : for( ; (((calpha_8)<=((cc - 1)))?1:0)!=0 ; )
               {
                  
                  {
                  
                  /* file sugi2.c line 177*/
                  if( 
                  /* file sugi2.c line 176*/
                  cmget(cm,ck_7,calpha_8)!=0)
                  {
                  	   
                     {
                     
                     /* file sugi2.c line 185*/
                     cbeta_9=(calpha_8 + 1);
                     lab_sugi27 : for( ; (((cbeta_9)<=(cc))?1:0)!=0 ; )
                     {
                        
                        {
                        
                        /* file sugi2.c line 182*/
                        if( 
                        /* file sugi2.c line 181*/
                        cmget(cm,cj_6,cbeta_9)!=0)
                        {
                        	   
                           {
                           cresult2_10++;
                           
                           }
                        
                        };
                        
                        }
                     cbeta_9++;
                     
                     }
                     
                     }
                  
                  };
                  
                  }
               calpha_8++;
               
               }
               
               }
            ck_7++;
            
            }
            
            }
         cj_6++;
         
         }
         retval= cresult2_10;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 200*/
   
   public static int cnumber_of_crossings_a(int  cmm)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ck_4= 0;
      int  ci_5= 0;
         
         {
         ck_4= 0;
         ci_5= 0;
         
         /* file sugi2.c line 207*/
         ci_5=0;
         lab_sugi28 : for( ; (((ci_5)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            ck_4=(ck_4 + 
            /* file sugi2.c line 206*/
            cnumber_of_crossings3(getMEMINT((int)((cmm + (ci_5*4)))),getMEMINT((int)((getMEMINT((int)((cmm + (ci_5*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + (ci_5*4)))) + 8)))));
            
            }
         ci_5++;
         
         }
         retval= ck_4;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
static int strsugi25= jtocstr("make_matrix(): initial matrix level %d (%dx%d)\n");
static int strsugi26= jtocstr("\nm->bits():\n");
static int strsugi27= jtocstr("0");
static int strsugi28= jtocstr("1");
static int strsugi29= jtocstr("\n");
   
   /* file sugi2.c line 215*/
   
   public static void cmake_matrix(int  cg,int  cl,int  cm)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int  cnl_5= 0;
      int  cel_6= 0;
      int  ci_7= 0;
      int  cj_8= 0;
      int  cc_9= 0;
      int  cr_10= 0;
         
         {
         cnl_5= 0;
         cel_6= 0;
         ci_7= 0;
         cj_8= 0;
         cc_9= 0;
         cr_10= 0;
         cnl_5=getMEMINT((int)((cg + 44)));
         lab_sugi29 : while( (cnl_5) !=0 )
         {
         	   
            {
            
            /* file sugi2.c line 231*/
            if( (((getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 28))))==(cl))?1:0)!=0)
            {
            	   
               {
               ci_7=getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 24)));
               setMEMINT((int)((getMEMINT((int)((cm + 16))) + (ci_7*4))),(int)(getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 0)))));
               
               }
            
            }
            else
            {
            	
            /* file sugi2.c line 237*/
            if( (((getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 28))))==((cl + 1)))?1:0)!=0)
            {
            	   
               {
               cj_8=getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 24)));
               setMEMINT((int)((getMEMINT((int)((cm + 24))) + (cj_8*4))),(int)(getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 0)))));
               
               }
            
            };
            
            };
            cnl_5=getMEMINT((int)((cnl_5 + 4)));
            
            }
         
         }
         cr_10=getMEMINT((int)((cm + 4)));
         cc_9=getMEMINT((int)((cm + 8)));
         
         /* file sugi2.c line 255*/
         ci_7=1;
         lab_sugi210 : for( ; (((ci_7)<=(cr_10))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 254*/
            cj_8=1;
            lab_sugi211 : for( ; (((cj_8)<=(cc_9))?1:0)!=0 ; )
            {
               
               {
               
               /* file sugi2.c line 253*/
               cmget_set(cm,ci_7,cj_8,0);
               
               }
            cj_8++;
            
            }
            
            }
         ci_7++;
         
         }
         cnl_5=getMEMINT((int)((cg + 44)));
         lab_sugi212 : while( (cnl_5) !=0 )
         {
         	   
            {
            
            /* file sugi2.c line 263*/
            if( (((getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 28))))==(cl))?1:0)!=0)
            {
            	   
               {
               cel_6=getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 46)));
               lab_sugi213 : while( (cel_6) !=0 )
               {
               	   
                  {
                  ci_7=getMEMINT((int)((getMEMINT((int)((cnl_5 + 0))) + 24)));
                  cj_8=getMEMINT((int)((getMEMINT((int)((getMEMINT((int)((cel_6 + 0))) + 8))) + 24)));
                  
                  /* file sugi2.c line 273*/
                  cmget_set(cm,ci_7,cj_8,1);
                  cel_6=getMEMINT((int)((cel_6 + 4)));
                  
                  }
               
               }
               
               }
            
            };
            cnl_5=getMEMINT((int)((cnl_5 + 4)));
            
            }
         
         }
         
         /* file sugi2.c line 282*/
         if( 0!=0)
         {
         	   
            {
            startargs(getMEMINT((int)((cm + 0))));
            addarg(getMEMINT((int)((cm + 4))));
            addarg(getMEMINT((int)((cm + 8))));
            
            /* file sugi2.c line 284*/
            cprintf(strsugi25,endargs());
            
            /* file sugi2.c line 285*/
            cfflush((int)((c_streamv + ((int)(1)*48))));
            
            /* file sugi2.c line 287*/
            cprintf(strsugi26,0);
            
            /* file sugi2.c line 288*/
            cfflush((int)((c_streamv + ((int)(1)*48))));
            cr_10=getMEMINT((int)((cm + 4)));
            cc_9=getMEMINT((int)((cm + 8)));
            
            /* file sugi2.c line 308*/
            ci_7=1;
            lab_sugi214 : for( ; (((ci_7)<=(cr_10))?1:0)!=0 ; )
            {
               
               {
               
               /* file sugi2.c line 305*/
               cj_8=1;
               lab_sugi215 : for( ; (((cj_8)<=(cc_9))?1:0)!=0 ; )
               {
                  
                  {
                  
                  /* file sugi2.c line 298*/
                  if( 
                  /* file sugi2.c line 297*/
                  cmget(cm,ci_7,cj_8)!=0)
                  {
                  	   
                     {
                     
                     /* file sugi2.c line 299*/
                     cprintf(strsugi27,0);
                     
                     }
                  
                  }
                  else
                  {
                  	   
                     {
                     
                     /* file sugi2.c line 303*/
                     cprintf(strsugi28,0);
                     
                     }
                  
                  };
                  
                  }
               cj_8++;
               
               }
               
               /* file sugi2.c line 306*/
               cprintf(strsugi29,0);
               
               /* file sugi2.c line 307*/
               cfflush((int)((c_streamv + ((int)(1)*48))));
               
               }
            ci_7++;
            
            }
            
            /* file sugi2.c line 309*/
            cprintf(strsugi29,0);
            
            /* file sugi2.c line 310*/
            cfflush((int)((c_streamv + ((int)(1)*48))));
            
            }
         
         };
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 321*/
   
   public static int csu_find_node_with_number(int  cg,int  cnr)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
         
         {
         
         /* file sugi2.c line 323*/
         if( cg!=0)
         {
         	
         };
         retval= 
         /* file sugi2.c line 325*/
         UndefFcs.cuniqnode(cnr);
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
static int strsugi210= jtocstr("node id=%d from x=%d to x=%d y=%d\n");
   
   /* file sugi2.c line 331*/
   
   public static void cstore_new_positions(int  cm,int  cl,int  cg)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int  cn_5= 0;
      int  ci_6= 0;
         
         {
         cn_5= (int)(0);
         ci_6= 0;
         
         /* file sugi2.c line 348*/
         ci_6=1;
         lab_sugi216 : for( ; (((ci_6)<=(getMEMINT((int)((cm + 4)))))?1:0)!=0 ; )
         {
            
            {
            cn_5=
            /* file sugi2.c line 338*/
            csu_find_node_with_number(cg,getMEMINT((int)((getMEMINT((int)((cm + 16))) + (ci_6*4)))));
            
            /* file sugi2.c line 340*/
            if( cn_5!=0)
            {
            	   
               {
               
               /* file sugi2.c line 342*/
               if( 0!=0)
               {
               	   
                  {
                  startargs(getMEMINT((int)((cn_5 + 4))));
                  addarg(getMEMINT((int)((cn_5 + 16))));
                  addarg(ci_6);
                  addarg(cl);
                  
                  /* file sugi2.c line 344*/
                  cprintf(strsugi210,endargs());
                  
                  }
               
               };
               setMEMINT((int)((cn_5 + 24)),(int)(ci_6));
               
               }
            
            };
            
            }
         ci_6++;
         
         }
         
         /* file sugi2.c line 363*/
         ci_6=1;
         lab_sugi217 : for( ; (((ci_6)<=(getMEMINT((int)((cm + 8)))))?1:0)!=0 ; )
         {
            
            {
            cn_5=
            /* file sugi2.c line 353*/
            csu_find_node_with_number(cg,getMEMINT((int)((getMEMINT((int)((cm + 24))) + (ci_6*4)))));
            
            /* file sugi2.c line 355*/
            if( cn_5!=0)
            {
            	   
               {
               
               /* file sugi2.c line 357*/
               if( 0!=0)
               {
               	   
                  {
                  startargs(getMEMINT((int)((cn_5 + 4))));
                  addarg(getMEMINT((int)((cn_5 + 16))));
                  addarg(ci_6);
                  addarg((cl + 1));
                  
                  /* file sugi2.c line 359*/
                  cprintf(strsugi210,endargs());
                  
                  }
               
               };
               setMEMINT((int)((cn_5 + 24)),(int)(ci_6));
               
               }
            
            };
            
            }
         ci_6++;
         
         }
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 370*/
   
   public static void ccopy_m(int  cm1,int  cm2)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
         
         {
         setMEMINT((int)((cm2 + 0)),(int)(getMEMINT((int)((cm1 + 0)))));
         setMEMINT((int)((cm2 + 4)),(int)(getMEMINT((int)((cm1 + 4)))));
         setMEMINT((int)((cm2 + 8)),(int)(getMEMINT((int)((cm1 + 8)))));
         setMEMINT((int)((cm2 + 12)),(int)(getMEMINT((int)((cm1 + 12)))));
         
         /* file sugi2.c line 376*/
         cmemmove(getMEMINT((int)((cm2 + 40))),getMEMINT((int)((cm1 + 40))),getMEMINT((int)((cm1 + 12))));
         
         /* file sugi2.c line 377*/
         cmemmove(getMEMINT((int)((cm2 + 16))),getMEMINT((int)((cm1 + 16))),getMEMINT((int)((cm1 + 20))));
         setMEMINT((int)((cm2 + 20)),(int)(getMEMINT((int)((cm1 + 20)))));
         
         /* file sugi2.c line 379*/
         cmemmove(getMEMINT((int)((cm2 + 24))),getMEMINT((int)((cm1 + 24))),getMEMINT((int)((cm1 + 28))));
         setMEMINT((int)((cm2 + 28)),(int)(getMEMINT((int)((cm1 + 28)))));
         setMEMINT((int)((cm2 + 32)),(int)(getMEMINT((int)((cm1 + 32)))));
         
         /* file sugi2.c line 382*/
         cmemmove(getMEMINT((int)((cm2 + 36))),getMEMINT((int)((cm1 + 36))),getMEMINT((int)((cm1 + 32))));
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 390*/
   
   public static int cequal_m(int  cm1,int  cm2,int  cr,int  cc)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_7= 0;
      int  cj_8= 0;
         
         {
         ci_7= 0;
         cj_8= 0;
         
         /* file sugi2.c line 403*/
         ci_7=1;
         lab_sugi218 : for( ; (((ci_7)<=(cr))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 402*/
            cj_8=1;
            lab_sugi219 : for( ; (((cj_8)<=(cc))?1:0)!=0 ; )
            {
               
               {
               
               /* file sugi2.c line 399*/
               if( (((
               /* file sugi2.c line 398*/
               cmget(cm1,ci_7,cj_8))!=(
               /* file sugi2.c line 398*/
               cmget(cm2,ci_7,cj_8)))?1:0)!=0)
               {
               	   
                  {
                  retval= 0;
                  if(true)
                  {
                  prevlevel();
                  return retval;
                  };
                  
                  }
               
               };
               
               }
            cj_8++;
            
            }
            
            }
         ci_7++;
         
         }
         retval= 1;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 411*/
   
   public static int cequal_a(int  cmm1,int  cmm2)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  cl_5= 0;
         
         {
         cl_5= 0;
         
         /* file sugi2.c line 422*/
         cl_5=0;
         lab_sugi220 : for( ; (((cl_5)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 419*/
            if( (((
            /* file sugi2.c line 417*/
            cequal_m(getMEMINT((int)((cmm1 + (cl_5*4)))),getMEMINT((int)((cmm2 + (cl_5*4)))),getMEMINT((int)((getMEMINT((int)((cmm1 + (cl_5*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm1 + (cl_5*4)))) + 8)))))==(0))?1:0)!=0)
            {
            	   
               {
               retval= 0;
               if(true)
               {
               prevlevel();
               return retval;
               };
               
               }
            
            };
            
            }
         cl_5++;
         
         }
         retval= 1;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 430*/
   
   public static void ccopy_a(int  cmm1,int  cmm2)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int  ci_4= 0;
         
         {
         ci_4= 0;
         
         /* file sugi2.c line 436*/
         ci_4=0;
         lab_sugi221 : for( ; (((ci_4)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 435*/
            ccopy_m(getMEMINT((int)((cmm1 + (ci_4*4)))),getMEMINT((int)((cmm2 + (ci_4*4)))));
            
            }
         ci_4++;
         
         }
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 444*/
   
   public static void cexch_rows(int  cm,int  cr1,int  cr2)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int  cj_5= 0;
      int  cid1_6= 0;
      int  cid2_7= 0;
      int  cbit1_8= 0;
      int  cbit2_9= 0;
         
         {
         cj_5= 0;
         cid1_6= 0;
         cid2_7= 0;
         cbit1_8= 0;
         cbit2_9= 0;
         cid1_6=getMEMINT((int)((getMEMINT((int)((cm + 16))) + (cr1*4))));
         cid2_7=getMEMINT((int)((getMEMINT((int)((cm + 16))) + (cr2*4))));
         setMEMINT((int)((getMEMINT((int)((cm + 16))) + (cr1*4))),(int)(cid2_7));
         setMEMINT((int)((getMEMINT((int)((cm + 16))) + (cr2*4))),(int)(cid1_6));
         
         /* file sugi2.c line 470*/
         cj_5=1;
         lab_sugi222 : for( ; (((cj_5)<=(getMEMINT((int)((cm + 8)))))?1:0)!=0 ; )
         {
            
            {
            cbit1_8=
            /* file sugi2.c line 466*/
            cmget(cm,cr1,cj_5);
            cbit2_9=
            /* file sugi2.c line 467*/
            cmget(cm,cr2,cj_5);
            
            /* file sugi2.c line 468*/
            cmget_set(cm,cr1,cj_5,cbit2_9);
            
            /* file sugi2.c line 469*/
            cmget_set(cm,cr2,cj_5,cbit1_8);
            
            }
         cj_5++;
         
         }
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 478*/
   
   public static void cexch_columns(int  cm,int  cc1,int  cc2)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int  ci_5= 0;
      int  cid1_6= 0;
      int  cid2_7= 0;
      int  cbit1_8= 0;
      int  cbit2_9= 0;
         
         {
         ci_5= 0;
         cid1_6= 0;
         cid2_7= 0;
         cbit1_8= 0;
         cbit2_9= 0;
         cid1_6=getMEMINT((int)((getMEMINT((int)((cm + 24))) + (cc1*4))));
         cid2_7=getMEMINT((int)((getMEMINT((int)((cm + 24))) + (cc2*4))));
         setMEMINT((int)((getMEMINT((int)((cm + 24))) + (cc1*4))),(int)(cid2_7));
         setMEMINT((int)((getMEMINT((int)((cm + 24))) + (cc2*4))),(int)(cid1_6));
         
         /* file sugi2.c line 504*/
         ci_5=1;
         lab_sugi223 : for( ; (((ci_5)<=(getMEMINT((int)((cm + 4)))))?1:0)!=0 ; )
         {
            
            {
            cbit1_8=
            /* file sugi2.c line 500*/
            cmget(cm,ci_5,cc1);
            cbit2_9=
            /* file sugi2.c line 501*/
            cmget(cm,ci_5,cc2);
            
            /* file sugi2.c line 502*/
            cmget_set(cm,ci_5,cc1,cbit2_9);
            
            /* file sugi2.c line 503*/
            cmget_set(cm,ci_5,cc2,cbit1_8);
            
            }
         ci_5++;
         
         }
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 511*/
   
   public static int creverse_r(int  cm,int  cr1,int  cr2)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_6= 0;
      int  cj_7= 0;
      int  cch_8= 0;
         
         {
         ci_6= 0;
         cj_7= 0;
         cch_8= 0;
         
         /* file sugi2.c line 520*/
         ci_6=cr1;
         cj_7=cr2;
         lab_sugi224 : for( ; (((ci_6)<(cj_7))?1:0)!=0 ; )
         {
            
            {
            cch_8++;
            
            /* file sugi2.c line 519*/
            cexch_rows(cm,ci_6,cj_7);
            
            }
         ci_6++;
         cj_7--;
         
         }
         retval= cch_8;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 528*/
   
   public static int creverse_c(int  cm,int  cc1,int  cc2)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_6= 0;
      int  cj_7= 0;
      int  cch_8= 0;
         
         {
         ci_6= 0;
         cj_7= 0;
         cch_8= 0;
         
         /* file sugi2.c line 537*/
         ci_6=cc1;
         cj_7=cc2;
         lab_sugi225 : for( ; (((ci_6)<(cj_7))?1:0)!=0 ; )
         {
            
            {
            cch_8++;
            
            /* file sugi2.c line 536*/
            cexch_columns(cm,ci_6,cj_7);
            
            }
         ci_6++;
         cj_7--;
         
         }
         retval= cch_8;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 545*/
   
   public static double crow_barycenter(int  cm,int  ci,int  cmaxval)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      double retval= 0;
      int  cj_6= 0;
      int  cr1_7= 0;
      int  cr2_8= 0;
         
         {
         cj_6= 0;
         cr1_7= 0;
         cr2_8= 0;
         
         /* file sugi2.c line 557*/
         cj_6=1;
         lab_sugi226 : for( ; (((cj_6)<=(cmaxval))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 553*/
            if( 
            /* file sugi2.c line 552*/
            cmget(cm,ci,cj_6)!=0)
            {
            	   
               {
               cr1_7=(cr1_7 + cj_6);
               cr2_8++;
               
               }
            
            };
            
            }
         cj_6++;
         
         }
         
         /* file sugi2.c line 560*/
         if( (((cr2_8)==(0))?1:0)!=0)
         {
         	   
            {
            retval= 0.0;
            if(true)
            {
            prevlevel();
            return retval;
            };
            
            }
         
         }
         else
         {
         	   
            {
            retval= ((double)(cr1_7)/(double)(cr2_8));
            if(true)
            {
            prevlevel();
            return retval;
            };
            
            }
         
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 572*/
   
   public static double ccolumn_barycenter(int  cm,int  cj,int  cmaxval)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      double retval= 0;
      int  ci_6= 0;
      int  cr1_7= 0;
      int  cr2_8= 0;
         
         {
         ci_6= 0;
         cr1_7= 0;
         cr2_8= 0;
         
         /* file sugi2.c line 584*/
         ci_6=1;
         lab_sugi227 : for( ; (((ci_6)<=(cmaxval))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 580*/
            if( 
            /* file sugi2.c line 579*/
            cmget(cm,ci_6,cj)!=0)
            {
            	   
               {
               cr1_7=(cr1_7 + ci_6);
               cr2_8++;
               
               }
            
            };
            
            }
         ci_6++;
         
         }
         
         /* file sugi2.c line 587*/
         if( (((cr2_8)==(0))?1:0)!=0)
         {
         	   
            {
            retval= 0.0;
            if(true)
            {
            prevlevel();
            return retval;
            };
            
            }
         
         }
         else
         {
         	   
            {
            retval= ((double)(cr1_7)/(double)(cr2_8));
            if(true)
            {
            prevlevel();
            return retval;
            };
            
            }
         
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 599*/
   
   public static int cr_r(int  cm1,int  cm2,int  cmax_r,int  cmax_c)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_7= 0;
      int  cj_8= 0;
      int  cch_9= 0;
         
         {
         ci_7= 0;
         cj_8= 0;
         cch_9= 0;
         
         /* file sugi2.c line 607*/
         ci_7=1;
         lab_sugi228 : for( ; (((ci_7)<=(cmax_r))?1:0)!=0 ; )
         {
            
            {
            setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_7*8))),(double)(
            /* file sugi2.c line 606*/
            crow_barycenter(cm1,ci_7,cmax_c)));
            
            }
         ci_7++;
         
         }
         
         /* file sugi2.c line 630*/
         ci_7=1;
         lab_sugi229 : for( ; (((ci_7)<(cmax_r))?1:0)!=0 ; )
         {
            
            {
            cj_8=ci_7;
            lab_sugi230 : while( (((((((cj_8)<(cmax_r))?1:0))!=0)? ((((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + ((cj_8 + 1)*8)))))==(getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (cj_8*8))))))?1:0)):0)) !=0 )
            {
            	   
               {
               cj_8++;
               
               }
            
            }
            
            /* file sugi2.c line 619*/
            if( (((cj_8)>(ci_7))?1:0)!=0)
            {
            	   
               {
               cch_9+=
               /* file sugi2.c line 620*/
               creverse_r(cm1,ci_7,cj_8);
               
               /* file sugi2.c line 623*/
               if( (((cm2)!=((int)(0)))?1:0)!=0)
               {
               	   
                  {
                  cch_9+=
                  /* file sugi2.c line 624*/
                  creverse_c(cm2,ci_7,cj_8);
                  
                  }
               
               };
               ci_7=cj_8;
               
               }
            
            };
            
            }
         ci_7++;
         
         }
         retval= cch_9;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 638*/
   
   public static int cr_c(int  cm1,int  cm2,int  cmax_r,int  cmax_c)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_7= 0;
      int  cj_8= 0;
      int  cch_9= 0;
         
         {
         ci_7= 0;
         cj_8= 0;
         cch_9= 0;
         
         /* file sugi2.c line 645*/
         if( cmax_r!=0)
         {
         	
         };
         
         /* file sugi2.c line 652*/
         ci_7=1;
         lab_sugi231 : for( ; (((ci_7)<=(cmax_c))?1:0)!=0 ; )
         {
            
            {
            setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_7*8))),(double)(
            /* file sugi2.c line 651*/
            ccolumn_barycenter(cm1,ci_7,cmax_c)));
            
            }
         ci_7++;
         
         }
         
         /* file sugi2.c line 675*/
         ci_7=1;
         lab_sugi232 : for( ; (((ci_7)<(cmax_c))?1:0)!=0 ; )
         {
            
            {
            cj_8=ci_7;
            lab_sugi233 : while( (((((((cj_8)<(cmax_c))?1:0))!=0)? ((((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + ((cj_8 + 1)*8)))))==(getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (cj_8*8))))))?1:0)):0)) !=0 )
            {
            	   
               {
               cj_8++;
               
               }
            
            }
            
            /* file sugi2.c line 664*/
            if( (((cj_8)>(ci_7))?1:0)!=0)
            {
            	   
               {
               cch_9+=
               /* file sugi2.c line 665*/
               creverse_c(cm1,ci_7,cj_8);
               
               /* file sugi2.c line 668*/
               if( (((cm2)!=((int)(0)))?1:0)!=0)
               {
               	   
                  {
                  cch_9+=
                  /* file sugi2.c line 669*/
                  creverse_r(cm2,ci_7,cj_8);
                  
                  }
               
               };
               ci_7=cj_8;
               
               }
            
            };
            
            }
         ci_7++;
         
         }
         retval= cch_9;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 684*/
   
   public static int cb_r(int  cm1,int  cm2,int  cmax_r,int  cmax_c)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      double  ctmpb_7= 0;
      int  ci_8= 0;
      int  cj_9= 0;
      int  ck_10= 0;
      int  cch_11= 0;
         
         {
         ctmpb_7= 0.0;
         ci_8= 0;
         cj_9= 0;
         ck_10= 0;
         cch_11= 0;
         
         /* file sugi2.c line 694*/
         ci_8=1;
         lab_sugi234 : for( ; (((ci_8)<=(cmax_r))?1:0)!=0 ; )
         {
            
            {
            setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8))),(double)(
            /* file sugi2.c line 693*/
            crow_barycenter(cm1,ci_8,cmax_c)));
            
            }
         ci_8++;
         
         }
         
         /* file sugi2.c line 727*/
         cj_9=cmax_r;
         lab_sugi235 : for( ; (((cj_9)>(1))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 699*/
            if( (((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (cj_9*8)))))!=(0.0))?1:0)!=0)
            {
            	   
               {
               
               /* file sugi2.c line 725*/
               ci_8=1;
               lab_sugi236 : for( ; (((ci_8)<(cj_9))?1:0)!=0 ; )
               {
                  
                  {
                  
                  /* file sugi2.c line 703*/
                  if( (((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8)))))!=(0.0))?1:0)!=0)
                  {
                  	   
                     {
                     ck_10=(ci_8 + 1);
                     lab_sugi237 : while( ((((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8)))))==(0.0))?1:0)) !=0 )
                     {
                     	   
                        {
                        ck_10++;
                        
                        }
                     
                     }
                     
                     /* file sugi2.c line 710*/
                     if( (((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8)))))>(getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8))))))?1:0)!=0)
                     {
                     	   
                        {
                        cch_11++;
                        ctmpb_7=getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8))));
                        setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8))),(double)(getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8))))));
                        setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8))),(double)(ctmpb_7));
                        
                        /* file sugi2.c line 716*/
                        cexch_rows(cm1,ci_8,ck_10);
                        
                        /* file sugi2.c line 718*/
                        if( (((cm2)!=((int)(0)))?1:0)!=0)
                        {
                        	   
                           {
                           cch_11++;
                           
                           /* file sugi2.c line 721*/
                           cexch_columns(cm2,ci_8,ck_10);
                           
                           }
                        
                        };
                        
                        }
                     
                     };
                     
                     }
                  
                  };
                  
                  }
               ci_8++;
               
               }
               
               }
            
            };
            
            }
         cj_9--;
         
         }
         retval= cch_11;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 736*/
   
   public static int cb_c(int  cm1,int  cm2,int  cmax_r,int  cmax_c)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      double  ctmpb_7= 0;
      int  ci_8= 0;
      int  cj_9= 0;
      int  ck_10= 0;
      int  cch_11= 0;
         
         {
         ctmpb_7= 0;
         ci_8= 0;
         cj_9= 0;
         ck_10= 0;
         cch_11= 0;
         
         /* file sugi2.c line 746*/
         ci_8=1;
         lab_sugi238 : for( ; (((ci_8)<=(cmax_c))?1:0)!=0 ; )
         {
            
            {
            setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8))),(double)(
            /* file sugi2.c line 745*/
            ccolumn_barycenter(cm1,ci_8,cmax_r)));
            
            }
         ci_8++;
         
         }
         
         /* file sugi2.c line 781*/
         cj_9=cmax_c;
         lab_sugi239 : for( ; (((cj_9)>(1))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 751*/
            if( (((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (cj_9*8)))))!=(0.0))?1:0)!=0)
            {
            	   
               {
               
               /* file sugi2.c line 779*/
               ci_8=1;
               lab_sugi240 : for( ; (((ci_8)<(cj_9))?1:0)!=0 ; )
               {
                  
                  {
                  
                  /* file sugi2.c line 755*/
                  if( (((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8)))))!=(0.0))?1:0)!=0)
                  {
                  	   
                     {
                     ck_10=(ci_8 + 1);
                     lab_sugi241 : while( ((((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8)))))==(0.0))?1:0)) !=0 )
                     {
                     	   
                        {
                        ck_10++;
                        
                        }
                     
                     }
                     
                     /* file sugi2.c line 764*/
                     if( (((getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8)))))>(getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8))))))?1:0)!=0)
                     {
                     	   
                        {
                        cch_11++;
                        ctmpb_7=getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8))));
                        setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ck_10*8))),(double)(getMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8))))));
                        setMEMDOUBLE((int)((getMEMINT((int)((cm1 + 36))) + (ci_8*8))),(double)(ctmpb_7));
                        
                        /* file sugi2.c line 771*/
                        cexch_columns(cm1,ci_8,ck_10);
                        
                        /* file sugi2.c line 773*/
                        if( (((cm2)!=((int)(0)))?1:0)!=0)
                        {
                        	   
                           {
                           cch_11++;
                           
                           /* file sugi2.c line 775*/
                           cexch_rows(cm2,ci_8,ck_10);
                           
                           }
                        
                        };
                        
                        }
                     
                     };
                     
                     }
                  
                  };
                  
                  }
               ci_8++;
               
               }
               
               }
            
            };
            
            }
         cj_9--;
         
         }
         retval= cch_11;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 790*/
   
   public static int csorted(int  cvector,int  cmaxval)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_5= 0;
         
         {
         ci_5= 0;
         
         /* file sugi2.c line 800*/
         ci_5=1;
         lab_sugi242 : for( ; (((ci_5)<(cmaxval))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 797*/
            if( ((((((getMEMDOUBLE((int)((cvector + (ci_5*8)))))>(getMEMDOUBLE((int)((cvector + ((ci_5 + 1)*8))))))?1:0))!=0)? ((((getMEMDOUBLE((int)((cvector + ((ci_5 + 1)*8)))))!=(0.0))?1:0)):0)!=0)
            {
            	   
               {
               retval= 0;
               if(true)
               {
               prevlevel();
               return retval;
               };
               
               }
            
            };
            
            }
         ci_5++;
         
         }
         retval= 1;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 808*/
   
   public static int cphase1_down(int  cmm)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_4= 0;
      int  cch_5= 0;
         
         {
         ci_4= 0;
         cch_5= 0;
         
         /* file sugi2.c line 816*/
         ci_4=0;
         lab_sugi243 : for( ; (((ci_4)<((getMEMINT((int)(cmaxlevel)) - 1)))?1:0)!=0 ; )
         {
            
            {
            cch_5+=
            /* file sugi2.c line 815*/
            cb_c(getMEMINT((int)((cmm + (ci_4*4)))),getMEMINT((int)((cmm + ((ci_4 + 1)*4)))),getMEMINT((int)((getMEMINT((int)((cmm + (ci_4*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + (ci_4*4)))) + 8))));
            
            }
         ci_4++;
         
         }
         cch_5+=
         /* file sugi2.c line 819*/
         cb_c(getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))),(int)(0),getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 8))));
         retval= cch_5;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 826*/
   
   public static int cphase1_up(int  cmm)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  ci_4= 0;
      int  cch_5= 0;
         
         {
         ci_4= 0;
         cch_5= 0;
         
         /* file sugi2.c line 835*/
         ci_4=(getMEMINT((int)(cmaxlevel)) - 1);
         lab_sugi244 : for( ; (((ci_4)>(0))?1:0)!=0 ; )
         {
            
            {
            cch_5+=
            /* file sugi2.c line 834*/
            cb_r(getMEMINT((int)((cmm + (ci_4*4)))),getMEMINT((int)((cmm + ((ci_4 - 1)*4)))),getMEMINT((int)((getMEMINT((int)((cmm + (ci_4*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + (ci_4*4)))) + 8))));
            
            }
         ci_4--;
         
         }
         cch_5+=
         /* file sugi2.c line 838*/
         cb_r(getMEMINT((int)((cmm + (0*4)))),(int)(0),getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 8))));
         retval= cch_5;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 847*/
   
   public static int cphase2_down(int  cmm)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  cl_4= 0;
      int  ci_5= 0;
      int  cch_6= 0;
         
         {
         cl_4= 0;
         ci_5= 0;
         cch_6= 0;
         
         /* file sugi2.c line 875*/
         cl_4=0;
         lab_sugi245 : for( ; (((cl_4)<((getMEMINT((int)(cmaxlevel)) - 1)))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 861*/
            ci_5=1;
            lab_sugi246 : for( ; (((ci_5)<=(getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 8)))))?1:0)!=0 ; )
            {
               
               {
               setMEMDOUBLE((int)((getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 36))) + (ci_5*8))),(double)(
               /* file sugi2.c line 860*/
               ccolumn_barycenter(getMEMINT((int)((cmm + (cl_4*4)))),ci_5,getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 4))))));
               
               }
            ci_5++;
            
            }
            
            /* file sugi2.c line 865*/
            if( (((
            /* file sugi2.c line 863*/
            csorted(getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 36))),getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 8)))))==(1))?1:0)!=0)
            {
            	   
               {
               cch_6+=
               /* file sugi2.c line 868*/
               cr_c(getMEMINT((int)((cmm + (cl_4*4)))),getMEMINT((int)((cmm + ((cl_4 + 1)*4)))),getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 8))));
               
               }
            
            }
            else
            {
            	   
               {
               retval= cch_6;
               if(true)
               {
               prevlevel();
               return retval;
               };
               
               }
            
            };
            
            }
         cl_4++;
         
         }
         
         /* file sugi2.c line 883*/
         ci_5=1;
         lab_sugi247 : for( ; (((ci_5)<=(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + (getMEMINT((int)(cmaxlevel))*4))))))?1:0)!=0 ; )
         {
            
            {
            setMEMDOUBLE((int)((getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 36))) + (ci_5*8))),(double)(
            /* file sugi2.c line 882*/
            ccolumn_barycenter(getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))),ci_5,getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 4))))));
            
            }
         ci_5++;
         
         }
         
         /* file sugi2.c line 889*/
         if( (((
         /* file sugi2.c line 887*/
         csorted(getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 36))),getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 8)))))==(1))?1:0)!=0)
         {
         	   
            {
            cch_6+=
            /* file sugi2.c line 893*/
            cr_c(getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))),(int)(0),getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))) + 8))));
            
            }
         
         };
         retval= cch_6;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
   
   /* file sugi2.c line 904*/
   
   public static int cphase2_up(int  cmm)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int retval= 0;
      int  cl_4= 0;
      int  ci_5= 0;
      int  cch_6= 0;
         
         {
         cl_4= 0;
         ci_5= 0;
         cch_6= 0;
         
         /* file sugi2.c line 931*/
         cl_4=(getMEMINT((int)(cmaxlevel)) - 1);
         lab_sugi248 : for( ; (((cl_4)>(0))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 917*/
            ci_5=1;
            lab_sugi249 : for( ; (((ci_5)<=(getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 4)))))?1:0)!=0 ; )
            {
               
               {
               setMEMDOUBLE((int)((getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 36))) + (ci_5*8))),(double)(
               /* file sugi2.c line 916*/
               crow_barycenter(getMEMINT((int)((cmm + (cl_4*4)))),ci_5,getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 8))))));
               
               }
            ci_5++;
            
            }
            
            /* file sugi2.c line 921*/
            if( (((
            /* file sugi2.c line 919*/
            csorted(getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 36))),getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 4)))))==(1))?1:0)!=0)
            {
            	   
               {
               cch_6+=
               /* file sugi2.c line 924*/
               cr_r(getMEMINT((int)((cmm + (cl_4*4)))),getMEMINT((int)((cmm + ((cl_4 - 1)*4)))),getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + (cl_4*4)))) + 8))));
               
               }
            
            }
            else
            {
            	   
               {
               retval= cch_6;
               if(true)
               {
               prevlevel();
               return retval;
               };
               
               }
            
            };
            
            }
         cl_4--;
         
         }
         
         /* file sugi2.c line 937*/
         ci_5=1;
         lab_sugi250 : for( ; (((ci_5)<=(getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 4)))))?1:0)!=0 ; )
         {
            
            {
            setMEMDOUBLE((int)((getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 36))) + (ci_5*8))),(double)(
            /* file sugi2.c line 936*/
            crow_barycenter(getMEMINT((int)((cmm + (0*4)))),ci_5,getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 8))))));
            
            }
         ci_5++;
         
         }
         
         /* file sugi2.c line 942*/
         if( (((
         /* file sugi2.c line 940*/
         csorted(getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 36))),getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 4)))))==(1))?1:0)!=0)
         {
         	   
            {
            cch_6+=
            /* file sugi2.c line 945*/
            cr_r(getMEMINT((int)((cmm + (0*4)))),(int)(0),getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 4))),getMEMINT((int)((getMEMINT((int)((cmm + (0*4)))) + 8))));
            
            }
         
         };
         retval= cch_6;
         if(true)
         {
         prevlevel();
         return retval;
         };
         
         }
      };
      prevlevel();
      return 0;
   }
static int strsugi211= jtocstr("bc_n++(): initial crossings is %d\n");
static int strsugi212= jtocstr("bc_n(++): current crossings phase1 is %d (%d:%d) ch1=%d cht=%d\n");
static int strsugi213= jtocstr("bc_n(++): current crossings phase2 is %d (%d:%d) ch1=%d ch2=%d cht=%d\n");
static int strsugi214= jtocstr("bc_n(++): final crossings is %d after %d changes made\n");
   
   /* file sugi2.c line 956*/
   
   public static void cbc_n(int  cg,int  cit1value,int  cit2value)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
      int  ca_5= 0;
      int  ca1_6= 0;
      int  ca2_7= 0;
      int  cas_8= 0;
      int  ci_9= 0;
      int  cks_10= 0;
      int  ck_11= 0;
      int  cn1_12= 0;
      int  cn2_13= 0;
      int  ccht_14= 0;
      int  cch1_15= 0;
      int  cch2_16= 0;
      int  cr1_17= 0;
      int  cr2_18= 0;
      int  cr3_19= 0;
      int  crr1_20= 0;
      int  crr2_21= 0;
      int  crr3_22= 0;
      int  cit1_23= 0;
      int  cit2_24= 0;
         
         {
         ca_5= (int)(0);
         ca1_6= (int)(0);
         ca2_7= (int)(0);
         cas_8= (int)(0);
         ci_9= 0;
         cks_10= 0;
         ck_11= 0;
         cn1_12= 0;
         cn2_13= 0;
         ccht_14= 0;
         cch1_15= 0;
         cch2_16= 0;
         cr1_17= 0;
         cr2_18= 0;
         cr3_19= 0;
         crr1_20= 0;
         crr2_21= 0;
         crr3_22= 0;
         cit1_23= 20;
         cit2_24= 40;
         
         /* file sugi2.c line 981*/
         if( (((cit1value)==(0))?1:0)!=0)
         {
         	   
            {
            cit1_23=20;
            
            }
         
         }
         else
         {
         	   
            {
            cit1_23=cit1value;
            
            }
         
         };
         
         /* file sugi2.c line 990*/
         if( (((cit2value)==(0))?1:0)!=0)
         {
         	   
            {
            cit2_24=40;
            
            }
         
         }
         else
         {
         	   
            {
            cit2_24=cit2value;
            
            }
         
         };
         ca_5=
         /* file sugi2.c line 1000*/
         ccalloc(1,(getMEMINT((int)(cmaxlevel))*4));
         ca1_6=
         /* file sugi2.c line 1001*/
         ccalloc(1,(getMEMINT((int)(cmaxlevel))*4));
         ca2_7=
         /* file sugi2.c line 1002*/
         ccalloc(1,(getMEMINT((int)(cmaxlevel))*4));
         cas_8=
         /* file sugi2.c line 1003*/
         ccalloc(1,(getMEMINT((int)(cmaxlevel))*4));
         
         /* file sugi2.c line 1012*/
         ci_9=0;
         lab_sugi251 : for( ; (((ci_9)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            setMEMINT((int)((ca_5 + (ci_9*4))),(int)(
            /* file sugi2.c line 1008*/
            ccalloc(1,44)));
            setMEMINT((int)((ca1_6 + (ci_9*4))),(int)(
            /* file sugi2.c line 1009*/
            ccalloc(1,44)));
            setMEMINT((int)((ca2_7 + (ci_9*4))),(int)(
            /* file sugi2.c line 1010*/
            ccalloc(1,44)));
            setMEMINT((int)((cas_8 + (ci_9*4))),(int)(
            /* file sugi2.c line 1011*/
            ccalloc(1,44)));
            
            }
         ci_9++;
         
         }
         
         /* file sugi2.c line 1094*/
         ci_9=0;
         lab_sugi252 : for( ; (((ci_9)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 0)),(int)(ci_9));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 0)),(int)(ci_9));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 0)),(int)(ci_9));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 0)),(int)(ci_9));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + (ci_9*4))))));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 4)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + (ci_9*4))))));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 4)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + (ci_9*4))))));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 4)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + (ci_9*4))))));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + ((ci_9 + 1)*4))))));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 8)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + ((ci_9 + 1)*4))))));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 8)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + ((ci_9 + 1)*4))))));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 8)),(int)(getMEMINT((int)((getMEMINT((int)(cnodes_of_level)) + ((ci_9 + 1)*4))))));
            
            /* file sugi2.c line 1037*/
            if( (((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4))))>(getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8)))))?1:0)!=0)
            {
            	   
               {
               setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4))) + 1)*8)));
               setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 4))) + 1)*8)));
               setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 4))) + 1)*8)));
               setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 4))) + 1)*8)));
               
               }
            
            }
            else
            {
            	   
               {
               setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8))) + 1)*8)));
               setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 8))) + 1)*8)));
               setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 8))) + 1)*8)));
               setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 32)),(int)(((getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 8))) + 1)*8)));
               
               }
            
            };
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 36)),(int)(
            /* file sugi2.c line 1051*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 32))))));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 36)),(int)(
            /* file sugi2.c line 1052*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 32))))));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 36)),(int)(
            /* file sugi2.c line 1053*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 32))))));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 36)),(int)(
            /* file sugi2.c line 1054*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 32))))));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 20)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 20)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 20)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 20)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 16)),(int)(
            /* file sugi2.c line 1063*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 20))))));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 16)),(int)(
            /* file sugi2.c line 1064*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 20))))));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 16)),(int)(
            /* file sugi2.c line 1065*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 20))))));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 16)),(int)(
            /* file sugi2.c line 1066*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 20))))));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 28)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 28)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 28)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 28)),(int)(((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8))) + 1)*4)));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 24)),(int)(
            /* file sugi2.c line 1075*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 28))))));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 24)),(int)(
            /* file sugi2.c line 1076*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 28))))));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 24)),(int)(
            /* file sugi2.c line 1077*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 28))))));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 24)),(int)(
            /* file sugi2.c line 1078*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 28))))));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 12)),(int)((1 + ((((getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 4))) + 1)*(getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 8))) + 1)) + 8)/8))));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 12)),(int)((1 + ((((getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 4))) + 1)*(getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 8))) + 1)) + 8)/8))));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 12)),(int)((1 + ((((getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 4))) + 1)*(getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 8))) + 1)) + 8)/8))));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 12)),(int)((1 + ((((getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 4))) + 1)*(getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 8))) + 1)) + 8)/8))));
            setMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 40)),(int)(
            /* file sugi2.c line 1090*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 12))))));
            setMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 40)),(int)(
            /* file sugi2.c line 1091*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 12))))));
            setMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 40)),(int)(
            /* file sugi2.c line 1092*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 12))))));
            setMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 40)),(int)(
            /* file sugi2.c line 1093*/
            ccalloc(1,getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 12))))));
            
            }
         ci_9++;
         
         }
         
         /* file sugi2.c line 1099*/
         ci_9=0;
         lab_sugi253 : for( ; (((ci_9)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 1098*/
            cmake_matrix(cg,ci_9,getMEMINT((int)((ca_5 + (ci_9*4)))));
            
            }
         ci_9++;
         
         }
         
         /* file sugi2.c line 1101*/
         ccopy_a(ca_5,cas_8);
         cks_10=
         /* file sugi2.c line 1102*/
         cnumber_of_crossings_a(cas_8);
         startargs(cks_10);
         
         /* file sugi2.c line 1104*/
         cprintf(strsugi211,endargs());
         setMEMINT((int)((cg + 60)),(int)(cks_10));
         
         /* file sugi2.c line 1112*/
         if( (((cks_10)>(0))?1:0)!=0)
         {
         	   
            {
            cch1_15=0;
            cch1_15+=
            /* file sugi2.c line 1118*/
            cphase1_down(ca_5);
            
            /* file sugi2.c line 1119*/
            ccopy_a(ca_5,cas_8);
            cch1_15+=
            /* file sugi2.c line 1120*/
            cphase1_up(ca_5);
            
            /* file sugi2.c line 1121*/
            ccopy_a(ca_5,cas_8);
            cn1_12=0;
            
            lab_sugi254 : do{
               
               {
               
               /* file sugi2.c line 1130*/
               ccopy_a(ca_5,ca1_6);
               cch1_15+=
               /* file sugi2.c line 1132*/
               cphase1_down(ca_5);
               ck_11=
               /* file sugi2.c line 1133*/
               cnumber_of_crossings_a(ca_5);
               
               /* file sugi2.c line 1136*/
               if( (((ck_11)<(cks_10))?1:0)!=0)
               {
               	   
                  {
                  cks_10=ck_11;
                  
                  /* file sugi2.c line 1139*/
                  ccopy_a(ca_5,cas_8);
                  
                  }
               
               };
               cch1_15+=
               /* file sugi2.c line 1142*/
               cphase1_up(ca_5);
               ck_11=
               /* file sugi2.c line 1144*/
               cnumber_of_crossings_a(ca_5);
               
               /* file sugi2.c line 1147*/
               if( (((ck_11)<(cks_10))?1:0)!=0)
               {
               	   
                  {
                  cks_10=ck_11;
                  
                  /* file sugi2.c line 1149*/
                  ccopy_a(ca_5,cas_8);
                  
                  }
               
               };
               ccht_14+=cch1_15;
               startargs(cks_10);
               addarg(cn1_12);
               addarg(cn2_13);
               addarg(cch1_15);
               addarg(ccht_14);
               
               /* file sugi2.c line 1156*/
               cprintf(strsugi212,endargs());
               
               /* file sugi2.c line 1159*/
               if( (((cks_10)==(0))?1:0)!=0)
               {
               	   
                  {
                  if(true)
                  {
                  break lab_sugi254;
                  };
                  
                  }
               
               };
               cr1_17=cr2_18;
               cr2_18=cr3_19;
               cr3_19=cks_10;
               
               /* file sugi2.c line 1168*/
               if( (((cr1_17)==(cr2_18))?1:0)!=0)
               {
               	   
                  {
                  
                  /* file sugi2.c line 1170*/
                  if( (((cr2_18)==(cr3_19))?1:0)!=0)
                  {
                  	   
                     {
                     if(true)
                     {
                     break lab_sugi254;
                     };
                     
                     }
                  
                  };
                  
                  }
               
               };
               
               }
            
            }while ((((((((++cn1_12))<(cit1_23))?1:0))!=0)? ((((
            /* file sugi2.c line 1176*/
            cequal_a(ca_5,ca1_6))==(0))?1:0)):0)!=0);
            
            /* file sugi2.c line 1180*/
            if( (((
            /* file sugi2.c line 1179*/
            cequal_a(ca_5,cas_8))==(0))?1:0)!=0)
            {
            	   
               {
               
               /* file sugi2.c line 1181*/
               ccopy_a(cas_8,ca_5);
               
               }
            
            };
            
            /* file sugi2.c line 1185*/
            if( (((cks_10)>(0))?1:0)!=0)
            {
            	   
               {
               cn2_13=0;
               ccht_14+=cch1_15;
               
               lab_sugi255 : do{
                  
                  {
                  cch2_16=0;
                  
                  /* file sugi2.c line 1194*/
                  ccopy_a(ca_5,ca2_7);
                  cch2_16+=
                  /* file sugi2.c line 1195*/
                  cphase2_down(ca_5);
                  cn1_12=0;
                  
                  lab_sugi256 : do{
                     
                     {
                     cch1_15=0;
                     
                     /* file sugi2.c line 1201*/
                     ccopy_a(ca_5,ca1_6);
                     cch1_15+=
                     /* file sugi2.c line 1202*/
                     cphase1_down(ca_5);
                     ck_11=
                     /* file sugi2.c line 1203*/
                     cnumber_of_crossings_a(ca_5);
                     
                     /* file sugi2.c line 1205*/
                     if( (((ck_11)<(cks_10))?1:0)!=0)
                     {
                     	   
                        {
                        cks_10=ck_11;
                        
                        /* file sugi2.c line 1207*/
                        ccopy_a(ca_5,cas_8);
                        
                        }
                     
                     };
                     cch1_15+=
                     /* file sugi2.c line 1210*/
                     cphase1_up(ca_5);
                     ck_11=
                     /* file sugi2.c line 1211*/
                     cnumber_of_crossings_a(ca_5);
                     
                     /* file sugi2.c line 1213*/
                     if( (((ck_11)<(cks_10))?1:0)!=0)
                     {
                     	   
                        {
                        cks_10=ck_11;
                        
                        /* file sugi2.c line 1215*/
                        ccopy_a(ca_5,cas_8);
                        
                        }
                     
                     };
                     
                     /* file sugi2.c line 1219*/
                     if( (((cks_10)==(0))?1:0)!=0)
                     {
                     	   
                        {
                        if(true)
                        {
                        break lab_sugi256;
                        };
                        
                        }
                     
                     };
                     crr1_20=crr2_21;
                     crr2_21=crr3_22;
                     crr3_22=cks_10;
                     
                     /* file sugi2.c line 1228*/
                     if( (((crr1_20)==(crr2_21))?1:0)!=0)
                     {
                     	   
                        {
                        
                        /* file sugi2.c line 1230*/
                        if( (((crr2_21)==(crr3_22))?1:0)!=0)
                        {
                        	   
                           {
                           if(true)
                           {
                           break lab_sugi256;
                           };
                           
                           }
                        
                        };
                        
                        }
                     
                     };
                     
                     }
                  
                  }while ((((((((++cn1_12))<(cit1_23))?1:0))!=0)? ((((
                  /* file sugi2.c line 1236*/
                  cequal_a(ca_5,ca1_6))==(0))?1:0)):0)!=0);
                  cch2_16+=
                  /* file sugi2.c line 1238*/
                  cphase2_up(ca_5);
                  cn1_12=0;
                  
                  lab_sugi257 : do{
                     
                     {
                     
                     /* file sugi2.c line 1243*/
                     ccopy_a(ca_5,ca1_6);
                     cch1_15+=
                     /* file sugi2.c line 1244*/
                     cphase1_up(ca_5);
                     ck_11=
                     /* file sugi2.c line 1245*/
                     cnumber_of_crossings_a(ca_5);
                     
                     /* file sugi2.c line 1247*/
                     if( (((ck_11)<(cks_10))?1:0)!=0)
                     {
                     	   
                        {
                        cks_10=ck_11;
                        
                        /* file sugi2.c line 1249*/
                        ccopy_a(ca_5,cas_8);
                        
                        }
                     
                     };
                     cch1_15+=
                     /* file sugi2.c line 1252*/
                     cphase1_down(ca_5);
                     ck_11=
                     /* file sugi2.c line 1253*/
                     cnumber_of_crossings_a(ca_5);
                     
                     /* file sugi2.c line 1255*/
                     if( (((ck_11)<(cks_10))?1:0)!=0)
                     {
                     	   
                        {
                        cks_10=ck_11;
                        
                        /* file sugi2.c line 1257*/
                        ccopy_a(ca_5,cas_8);
                        
                        }
                     
                     };
                     ccht_14+=cch1_15;
                     startargs(cks_10);
                     addarg(cn1_12);
                     addarg(cn2_13);
                     addarg(cch1_15);
                     addarg(cch2_16);
                     addarg(ccht_14);
                     
                     /* file sugi2.c line 1264*/
                     cprintf(strsugi213,endargs());
                     
                     /* file sugi2.c line 1265*/
                     cfflush((int)((c_streamv + ((int)(1)*48))));
                     
                     /* file sugi2.c line 1268*/
                     if( (((cks_10)==(0))?1:0)!=0)
                     {
                     	   
                        {
                        if(true)
                        {
                        break lab_sugi257;
                        };
                        
                        }
                     
                     };
                     crr1_20=crr2_21;
                     crr2_21=crr3_22;
                     crr3_22=cks_10;
                     
                     /* file sugi2.c line 1277*/
                     if( (((crr1_20)==(crr2_21))?1:0)!=0)
                     {
                     	   
                        {
                        
                        /* file sugi2.c line 1279*/
                        if( (((crr2_21)==(crr3_22))?1:0)!=0)
                        {
                        	   
                           {
                           if(true)
                           {
                           break lab_sugi257;
                           };
                           
                           }
                        
                        };
                        
                        }
                     
                     };
                     
                     }
                  
                  }while ((((((((++cn1_12))<(cit1_23))?1:0))!=0)? ((((
                  /* file sugi2.c line 1287*/
                  cequal_a(ca_5,ca1_6))==(0))?1:0)):0)!=0);
                  ccht_14+=cch1_15;
                  ccht_14+=cch2_16;
                  
                  /* file sugi2.c line 1294*/
                  if( (((cks_10)==(0))?1:0)!=0)
                  {
                  	   
                     {
                     if(true)
                     {
                     break lab_sugi255;
                     };
                     
                     }
                  
                  };
                  cr1_17=cr2_18;
                  cr2_18=cr3_19;
                  cr3_19=cks_10;
                  
                  /* file sugi2.c line 1303*/
                  if( (((cr1_17)==(cr2_18))?1:0)!=0)
                  {
                  	   
                     {
                     
                     /* file sugi2.c line 1305*/
                     if( (((cr2_18)==(cr3_19))?1:0)!=0)
                     {
                     	   
                        {
                        if(true)
                        {
                        break lab_sugi255;
                        };
                        
                        }
                     
                     };
                     
                     }
                  
                  };
                  
                  }
               
               }while ((((((((++cn2_13))<(cit2_24))?1:0))!=0)? ((((
               /* file sugi2.c line 1311*/
               cequal_a(ca_5,ca2_7))==(0))?1:0)):0)!=0);
               
               }
            
            };
            
            }
         
         };
         setMEMINT((int)((cg + 64)),(int)(cks_10));
         setMEMINT((int)((cg + 68)),(int)(ccht_14));
         startargs(cks_10);
         addarg(ccht_14);
         
         /* file sugi2.c line 1324*/
         cprintf(strsugi214,endargs());
         
         /* file sugi2.c line 1325*/
         cfflush((int)((c_streamv + ((int)(1)*48))));
         
         /* file sugi2.c line 1331*/
         ci_9=0;
         lab_sugi258 : for( ; (((ci_9)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 1330*/
            cstore_new_positions(getMEMINT((int)((cas_8 + (ci_9*4)))),ci_9,cg);
            
            }
         ci_9+=2;
         
         }
         
         /* file sugi2.c line 1334*/
         if( (((ci_9)==(getMEMINT((int)(cmaxlevel))))?1:0)!=0)
         {
         	   
            {
            
            /* file sugi2.c line 1335*/
            cstore_new_positions(getMEMINT((int)((cas_8 + ((getMEMINT((int)(cmaxlevel)) - 1)*4)))),(getMEMINT((int)(cmaxlevel)) - 1),cg);
            
            }
         
         };
         
         /* file sugi2.c line 1364*/
         ci_9=0;
         lab_sugi259 : for( ; (((ci_9)<(getMEMINT((int)(cmaxlevel))))?1:0)!=0 ; )
         {
            
            {
            
            /* file sugi2.c line 1340*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 36))));
            
            /* file sugi2.c line 1341*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 36))));
            
            /* file sugi2.c line 1342*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 36))));
            
            /* file sugi2.c line 1343*/
            cfree(getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 36))));
            
            /* file sugi2.c line 1345*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 16))));
            
            /* file sugi2.c line 1346*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 16))));
            
            /* file sugi2.c line 1347*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 16))));
            
            /* file sugi2.c line 1348*/
            cfree(getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 16))));
            
            /* file sugi2.c line 1350*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 24))));
            
            /* file sugi2.c line 1351*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 24))));
            
            /* file sugi2.c line 1352*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 24))));
            
            /* file sugi2.c line 1353*/
            cfree(getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 24))));
            
            /* file sugi2.c line 1355*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca_5 + (ci_9*4)))) + 40))));
            
            /* file sugi2.c line 1356*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca1_6 + (ci_9*4)))) + 40))));
            
            /* file sugi2.c line 1357*/
            cfree(getMEMINT((int)((getMEMINT((int)((ca2_7 + (ci_9*4)))) + 40))));
            
            /* file sugi2.c line 1358*/
            cfree(getMEMINT((int)((getMEMINT((int)((cas_8 + (ci_9*4)))) + 40))));
            
            /* file sugi2.c line 1360*/
            cfree(getMEMINT((int)((ca_5 + (ci_9*4)))));
            
            /* file sugi2.c line 1361*/
            cfree(getMEMINT((int)((ca1_6 + (ci_9*4)))));
            
            /* file sugi2.c line 1362*/
            cfree(getMEMINT((int)((ca2_7 + (ci_9*4)))));
            
            /* file sugi2.c line 1363*/
            cfree(getMEMINT((int)((cas_8 + (ci_9*4)))));
            
            }
         ci_9++;
         
         }
         
         /* file sugi2.c line 1366*/
         cfree(ca_5);
         
         /* file sugi2.c line 1367*/
         cfree(ca1_6);
         
         /* file sugi2.c line 1368*/
         cfree(ca2_7);
         
         /* file sugi2.c line 1369*/
         cfree(cas_8);
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }
   
   /* file sugi2.c line 1378*/
   
   public static void creduce_crossings2(int  cg,int  cit1v,int  cit2v)
   {
      nextlevel();
      if(true)
      {
      int label= 0;
         
         {
         
         /* file sugi2.c line 1380*/
         cbc_n(cg,cit1v,cit2v);
         if(true)
         {
         prevlevel();
         return ;
         };
         
         }
      };
      prevlevel();
   }

}
