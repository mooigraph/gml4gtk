/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  This includes rhp.c Copyright (C) 2008, 2011 Matthias Stallmann, Saurabh Gupta.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#include "config.h"

#include <stdio.h>
#include <string.h>

#include "splay-tree.h"
#include "main.h"
#include "dpmem.h"
#include "uniqnode.h"

/* by uniq number of node */
static splay_tree uniqnode_splaytree = NULL;

/* by uniq gml number of node */
static splay_tree uniqnodeid_splaytree = NULL;

/* add node */
void uniqnode_add(struct gml_graph *g, struct gml_node *node)
{
  splay_tree_node spn = NULL;

  if (uniqnode_splaytree == NULL) {
    uniqnode_splaytree = splay_tree_new(splay_tree_compare_ints, NULL, NULL);
  }

  if (uniqnodeid_splaytree == NULL) {
    uniqnodeid_splaytree = splay_tree_new(splay_tree_compare_ints, NULL, NULL);
  }

  /* use uniq node number starting at 1, gml number is in id */
  spn = splay_tree_lookup(uniqnode_splaytree, (splay_tree_key) node->nr);
  if (spn) {
    /* shouldnothappen */
    printf("%s(): 1 node %d with gml id %d does already exist\n", __func__, node->nr, node->id);
    fflush(stdout);
    return;
  }

  /* use uniq node number starting at 1, gml number is in id */
  spn = splay_tree_lookup(uniqnodeid_splaytree, (splay_tree_key) node->id);
  if (spn) {
    /* shouldnothappen */
    printf("%s(): 2 node %d with gml id %d does already exist\n", __func__, node->nr, node->id);
    fflush(stdout);
    return;
  }

  /* by internal uniq number */
  splay_tree_insert(uniqnode_splaytree, (splay_tree_key) node->nr, (splay_tree_value) node);
  /* by gml id number */
  splay_tree_insert(uniqnodeid_splaytree, (splay_tree_key) node->id, (splay_tree_value) node);

  return;
}

/* find the node by uniq number */
struct gml_node *uniqnode(struct gml_graph *g, int nr)
{
  splay_tree_node spn = NULL;
  if (uniqnode_splaytree == NULL) {
    /* shouldnothappen */
    return NULL;
  }
  /* use uniq node number starting at 1, gml number is in id */
  spn = splay_tree_lookup(uniqnode_splaytree, (splay_tree_key) nr);
  if (!spn) {
    /* not found */
    return (NULL);
  }
  return ((struct gml_node *)spn->value);
}

/* find the node by uniq gml number */
struct gml_node *uniqnodeid(struct gml_graph *g, int nr)
{
  splay_tree_node spn = NULL;
  if (uniqnodeid_splaytree == NULL) {
    /* shouldnothappen */
    return NULL;
  }
  /* use uniq node number starting at 1, gml number is in id */
  spn = splay_tree_lookup(uniqnodeid_splaytree, (splay_tree_key) nr);
  if (!spn) {
    /* not found */
    return (NULL);
  }
  return ((struct gml_node *)spn->value);
}

/* end. */
