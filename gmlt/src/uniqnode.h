/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  This includes Copyright (C) 2008, 2011 Matthias Stallmann, Saurabh Gupta.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef UNIQNODE_H
#define UNIQNODE_H 1

/* add node with internal and gml number */
extern void uniqnode_add(struct gml_graph *g, struct gml_node *node);

/* find the node by internal number */
extern struct gml_node *uniqnode(struct gml_graph *g, int nr);

/* find the node by uniq gml number */
extern struct gml_node *uniqnodeid(struct gml_graph *g, int nr);

#endif

/* end. */
