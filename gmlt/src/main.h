/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  This includes Copyright (C) 2008, 2011 Matthias Stallmann, Saurabh Gupta.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef MAIN_H
#define MAIN_H 1

/* same data structures only cut down to what is needed. */
struct gml_graph;
struct gml_node;
struct gml_edge;
struct gml_nlist;
struct gml_elist;
struct gml_rl;
struct gml_hl;

/* */
struct gml_graph {
  int id;			/* uniq number and 0 is rootgraph */

  int nnodes;			/* number of input nodes */
  int nedges;			/* number of input edges */

  int nodenum;			/* node uniq number starting at 1 */
  int edgenum;			/* edge uniq number starting at 1 */

  /* the raw parsed node/edge list */
  struct gml_nlist *rawnodelist;
  struct gml_nlist *rawnodelisttail;
  struct gml_elist *rawedgelist;
  struct gml_elist *rawedgelisttail;
  /* */

  /* the used and changed node/edge/single-node list */
  struct gml_nlist *nodelist;
  struct gml_nlist *nodelisttail;
  struct gml_elist *edgelist;
  struct gml_elist *edgelisttail;

  /* changed: the deleted edges */
  struct gml_elist *dedgelist;
  struct gml_elist *dedgelisttail;

};

/* */
struct gml_node {
  int nr;			/* uniq node number starting at 1 */
  int id;			/* id number as in gml graph */
  char *name;			/* name of node or label */
  char *nlabel;			/* optional label or the id */
};

/* linkage of nodes. */
struct gml_nlist {
  struct gml_node *node;
  struct gml_nlist *next;
};

/* */
struct gml_edge {
  int nr;			/* uniq number staring at 1 */
  struct gml_node *from_node;	/* from node */
  struct gml_node *to_node;	/* to node */

};

/* linkage of edges */
struct gml_elist {
  struct gml_edge *edge;
  struct gml_elist *next;
};

/* this is dot only and not used here */
struct gml_rl {
  int n;
};

/* this is dot only and not used here */
struct gml_hl {
  int n;
};

/* the graph data rootgraph */
extern struct gml_graph *maingraph;

/* in main.c turn on/off debug output */
extern int yydebug;

/* in gml.c peg parser */
extern int gmlmain(FILE * gmlstream, int gmldebug);

#endif

/* end. */
