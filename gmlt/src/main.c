/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  This includes rhp.c Copyright (C) 2008, 2011 Matthias Stallmann, Saurabh Gupta.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#include "config.h"

#include <stdio.h>

#include "splay-tree.h"
#include "main.h"
#include "dpmem.h"
#include "hier.h"

/* this depends on the gui or drawing code */
static void gml_textsizes(struct gml_graph *g);

/* turn on/off extra debug output */
int yydebug = 1;

int main(int argc, char *argv[])
{
  int status = 0;
  FILE *fstream = NULL;
  struct gml_graph *g = NULL;

  /* parse the gml graph data using peg parser */
  fstream = fopen(argv[0], "r");
  printf("%s(): file %s\n", __func__, argv[1]);
  status = gmlmain(fstream, 0 /* yydebug */ );
  fclose(fstream);

  printf("parserstatus=%d (0 means it parsed oke)\n", status);

  /* stop at parse error */
  if (status) {
    return (1);
  }

  /* for this only one main graph */
  g = maingraph;

  /* prepare graph data */
  prep(g);

  /* create data about edges with labels */
  prepel(g);

  /* remove cycles in the graph */
  uncycle(g);

  /* relative y levels of nodes */
  ylevels(g);

  /* check for same edges */
  checksame(g);

  /* get (x,y) size of node labels */
  gml_textsizes(g);

  /* run GNU GPL rhp.c barycenter from matthias stallman */
  barycenter(g, 0, 0);

  /* doublespace relative y levels after barycenter */
  doublespaceyafter(g);

  /* split longer edges */
  splitedgesafter(g);

  /* add edgelabels */
  edgelabelsafter(g);

  /* do absolute node positioning using different ways */
  improve_positions(g);

  /* do additional edge line routing */
  edgerouting(g);

  /* create some image, svg, png or other */
  createimage(g);

  return (0);
}

/* how much space is needed for the text label at 1:1 scale in pixels
 * just count how many lines and how long longest text line is
 */
static void gml_textsizes(struct gml_graph *g)
{
  return;
}

/* end. */
