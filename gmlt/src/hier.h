/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *  also Copyright (C) 2008, 2011 Matthias Stallmann, Saurabh Gupta.
 *  See also https://github.com/mfms-ncsu
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef HIER_H
#define HIER_H 1

/* */
extern void add_new_node(struct gml_graph *g, struct gml_graph *ro, int nr, int foundid, char *nodename, char *nodelabel, int ncolor, int nbcolor, struct gml_rl *rlabel, struct gml_hl *hlabel, int fontcolor, int ishtml);

/* */
extern void add_new_edge(struct gml_graph *g, struct gml_graph *ro, int foundsource, int foundtarget, char *elabel, int ecolor, int style, char *fcompass, char *tcompass, int constraint, int ishtml);

/* delete edge */
extern void del_edge(struct gml_graph *g, struct gml_elist *edgeel);

/* prepare for all-at-once graph layout */
extern void prep(struct gml_graph *g);

/* prepare the data about edge labels */
extern void prepel(struct gml_graph *g);

/* remove cycles in the graph */
extern void uncycle(struct gml_graph *g);

/* set relative y level of all nodes */
extern void ylevels(struct gml_graph *g);

/* check for same edges */
extern void checksame(struct gml_graph *g);

/* run barycenter */
extern void barycenter(struct gml_graph *g, int it1val, int it2val);

/* doublespace the vertical levels */
extern void doublespaceyafter(struct gml_graph *g);

/* add edgelabels */
extern void edgelabelsafter(struct gml_graph *g);

/* split longer edges */
extern void splitedgesafter(struct gml_graph *g);

/* run positioning */
extern void improve_positions(struct gml_graph *g);

/* do additional edge line routing */
extern void edgerouting(struct gml_graph *g);

/* create some image, svg, png or other */
extern void createimage(struct gml_graph *g);

#endif

/* end. */
