/*
 *  Copyright 2021 籽籮籮 籵籮籮籯类籲籷籰
 *  This includes Copyright (C) 2008, 2011 Matthias Stallmann, Saurabh Gupta.
 *  See also https://github.com/mfms-ncsu
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#include "config.h"

#include <stdio.h>
#include <string.h>

#include "splay-tree.h"
#include "main.h"
#include "hier.h"
#include "dpmem.h"
#include "uniqstr.h"
#include "uniqnode.h"

/* the graph data rootgraph */
struct gml_graph *maingraph = NULL;

/* */
void add_new_node(struct gml_graph *g, struct gml_graph *ro, int nr, int foundid, char *nodename, char *nodelabel, int ncolor, int nbcolor, struct gml_rl *rlabel, struct gml_hl *hlabel, int fontcolor, int ishtml)
{
  struct gml_node *node = NULL;
  struct gml_nlist *lnll = NULL;
  char nlbuf[16];

  printf("%s(): adding node %d foundid=%d \"%s\" with label \"%s\"\n", __func__, nr, foundid, nodename, nodelabel);

  /* label is node number or specified label */
  if (nodelabel == NULL) {
    memset(nlbuf, 0, 16);
    /* no specified node label, so use the id of the node. */
    snprintf(nlbuf, (16 - 1), "%d", foundid);
  }

  /* add here check for double defined nodes */

  node = dp_calloc(1, sizeof(struct gml_node));

  /* layouter number */
  node->nr = nr;

  /* gml number */
  node->id = foundid;

  node->name = uniqstr(nodename);

  if (nodelabel) {
    /* label is the specified label */
    node->nlabel = uniqstr(nodelabel);
  } else {
    /* label is the numeric string of the node id. */
    node->nlabel = uniqstr(nlbuf);
  }

  lnll = dp_calloc(1, sizeof(struct gml_nlist));

  lnll->node = node;

  if (g->rawnodelist == NULL) {
    g->rawnodelist = lnll;
    g->rawnodelisttail = lnll;
  } else {
    g->rawnodelisttail->next = lnll;
    g->rawnodelisttail = lnll;
  }

  /* number of nodes in this graph */
  g->nnodes++;

  /* add uniq node by internal number and uniq gml number */
  uniqnode_add(g, node);

  return;
}

/* the numbers are the gml numbers. */
void add_new_edge(struct gml_graph *g, struct gml_graph *ro, int foundsource, int foundtarget, char *elabel, int ecolor, int style, char *fcompass, char *tcompass, int constraint, int ishtml)
{
  struct gml_edge *edge = NULL;
  struct gml_elist *el = NULL;
  struct gml_node *snode = NULL;
  struct gml_node *tnode = NULL;

  printf("%s(): adding edge from %d to %d\n", __func__, foundsource, foundtarget);

  /* add here the check if nodes are define with uniqnode() */
  snode = uniqnodeid(g, foundsource);

  if (!snode) {
    /* shouldnothappen */
    printf("%s(): source node with number %d not found\n", __func__, foundsource);
    return;
  }

  tnode = uniqnodeid(g, foundtarget);

  if (!tnode) {
    /* shouldnothappen */
    printf("%s(): target node with number %d not found\n", __func__, foundtarget);
    return;
  }

  /* a new edge */
  edge = dp_calloc(1, sizeof(struct gml_edge));

  /* edge number start at 1 */
  g->edgenum++;
  edge->nr = g->edgenum;

  /* */
  el = dp_calloc(1, sizeof(struct gml_elist));

  el->edge = edge;

  if (g->rawedgelist == NULL) {
    g->rawedgelist = el;
    g->rawedgelisttail = el;
  } else {
    g->rawedgelisttail->next = el;
    g->rawedgelisttail = el;
  }

  /* number of edges in this subgraph */
  g->nedges++;

  return;
}

/* delete edge */
void del_edge(struct gml_graph *g, struct gml_elist *edgeel)
{
  /* changed: not delete edge but put it in another edge list */
  return;
}

/* prepare for all-at-once graph layout */
void prep(struct gml_graph *g)
{
  struct gml_nlist *lnl = NULL;
  struct gml_elist *enl = NULL;

  printf("%s(): layouter data:\n", __func__);
  lnl = maingraph->rawnodelist;
  while (lnl) {
    printf("  node %d\n", lnl->node->nr);
    lnl = lnl->next;
  }

  enl = maingraph->rawedgelist;
  while (enl) {
    printf("  edge %d\n", enl->edge->nr);
    enl = enl->next;
  }

  return;
}

/* prepare the data about edge labels */
void prepel(struct gml_graph *g)
{
  return;
}

/* remove cycles in the graph */
void uncycle(struct gml_graph *g)
{
  /* run dfs to mark edges to reverse */
  /* after that do actual reverse of the edges */
  /* indicate the start nodes now with no incoming edges */
  return;
}

/* set relative y level of all nodes */
void ylevels(struct gml_graph *g)
{
  /* run dfs to set the relative y level.
   * start with the nodes which are start nodes
   */
  return;
}

/* check for same edges as 1->2 and 1->2 */
void checksame(struct gml_graph *g)
{
  return;
}

/* run barycenter using rhp.c from matthias stallman */
void barycenter(struct gml_graph *g, int it1val, int it2val)
{
  /* copy data to rhp.c and run and copy result back */
  printf("%s(): running rhp.c barycenter\n", __func__);
  return;
}

/* doublespace the vertical levels */
void doublespaceyafter(struct gml_graph *g)
{
  printf("%s():\n", __func__);
  return;
}

/* split longer edges */
void splitedgesafter(struct gml_graph *g)
{
  printf("%s():\n", __func__);
  return;
}

/* add edgelabels */
void edgelabelsafter(struct gml_graph *g)
{
  printf("%s():\n", __func__);
  return;
}

/* run positioning */
void improve_positions(struct gml_graph *g)
{
  printf("%s():\n", __func__);
  /* add here own algorithm as easy as possible or as complex as needed */
  return;
}

/* do additional edge line routing */
void edgerouting(struct gml_graph *g)
{
  printf("%s():\n", __func__);
  return;
}

/* create some image, svg, png or other */
void createimage(struct gml_graph *g)
{
  printf("%s():\n", __func__);
  return;
}

/* end. */
