# only parser test data
graph
[
    node
    [
	id 0
	label "label0"
	graphics [ baz  bar ]
    ]
    node
    [
	id 1
	graphics [ baz  bar ]
    ]
    edge [
	source 0 target 1
	graphics [
	    baz  bar
	    foo [
		yo 2
	    ]
	]
    ]
    edge [
	source 1 target 0 label "edge10"
    ]
    graphics [ baz  bar ]
]
