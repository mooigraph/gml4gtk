
# generated with kegg2gml.xsl
graph [
 directed 1

 node [ id 26 label "ko:K03119" ]

 node [ id 27 label "path:hsa00920" ]

 node [ id 28 label "ko:K05888" ]

 node [ id 30 label "ko:K00259" ]

 node [ id 31 label "cpd:C00094" ]

 node [ id 32 label "cpd:C06735" ]

 node [ id 33 label "cpd:C00041" ]

 node [ id 34 label "cpd:C00022" ]

 node [ id 35 label "cpd:C14179" ]

 node [ id 36 label "ko:K13788 ko:K00625 ko:K15024" ]

 node [ id 37 label "cpd:C00227" ]

 node [ id 38 label "ko:K03852" ]

 node [ id 39 label "path:hsa00620" ]

 node [ id 40 label "cpd:C00024" ]

 node [ id 41 label "ko:K03851" ]

 node [ id 42 label "path:hsa00430" ]

 node [ id 43 label "path:hsa00480" ]

 node [ id 44 label "path:hsa00460" ]

 node [ id 45 label "path:hsa00270" ]

 node [ id 46 label "ko:K07255 ko:K07256" ]

 node [ id 48 label "hsa:51380" ]

 node [ id 49 label "hsa:2571 hsa:2572" ]

 node [ id 52 label "hsa:1036" ]

 node [ id 53 label "hsa:84890" ]

 node [ id 54 label "hsa:51380" ]

 node [ id 55 label "hsa:2571 hsa:2572" ]

 node [ id 56 label "hsa:124975 hsa:2678 hsa:2686 hsa:2687" ]

 node [ id 57 label "hsa:570" ]

 node [ id 58 label "ko:K00925" ]

 node [ id 59 label "cpd:C00606" ]

 node [ id 60 label "cpd:C00519" ]

 node [ id 61 label "cpd:C01678" ]

 node [ id 62 label "cpd:C00097" ]

 node [ id 63 label "cpd:C00506" ]

 node [ id 64 label "cpd:C00245" ]

 node [ id 65 label "cpd:C00593" ]

 node [ id 66 label "cpd:C05123" ]

 node [ id 67 label "cpd:C01959" ]

 node [ id 68 label "cpd:C03149" ]

 node [ id 69 label "cpd:C05844" ]

 node [ id 70 label "cpd:C05122" ]

 node [ id 71 label "cpd:C00033" ]

 node [ id 78 label "ko:K15373" ]

 node [ id 79 label "ko:K15372" ]

 node [ id 80 label "ko:K00260 ko:K15371" ]

 node [ id 81 label "cpd:C00025" ]

 node [ id 82 label "cpd:C00026" ]

 edge [ source 53 target 55 ]

 edge [ source 53 target 54 ]

 edge [ source 49 target 57 ]

 edge [ source 49 target 56 ]

 edge [ source 48 target 57 ]

 edge [ source 48 target 56 ]

 edge [ source 56 target 57 ]

 edge [ source 52 target 55 ]

 edge [ source 52 target 54 ]

 edge [ source 52 target 43 ]

 edge [ source 52 target 44 ]

 edge [ source 43 target 44 ]

 edge [ source 52 target 45 ]

 edge [ source 55 target 45 ]

 edge [ source 54 target 45 ]

 edge [ source 49 target 45 ]

 edge [ source 48 target 45 ]

]
