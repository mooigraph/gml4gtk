<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0"
	>
<xsl:output method="text"/>

<xsl:template match="/">
# generated with kegg2gml.xsl
graph [
 directed 1
<xsl:apply-templates select="pathway"/>
]
</xsl:template>


<xsl:template match="pathway">
		<xsl:for-each select="entry">
			<xsl:if test="position()&gt;1"></xsl:if>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		<xsl:for-each select="relation">
			<xsl:if test="position()&gt;1"></xsl:if>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
</xsl:template>

<xsl:template match="entry">
 node [ id <xsl:value-of select="@id"/> label "<xsl:value-of select="@name"/>" ]
</xsl:template>

<xsl:template match="relation">
 edge [ source <xsl:value-of select="@entry1"/> target <xsl:value-of select="@entry2"/> ]
</xsl:template>

</xsl:stylesheet>
