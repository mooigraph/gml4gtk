<?xml version="1.0"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0"
	>
<xsl:output method="text"/>

<xsl:template match="/">
{
<xsl:apply-templates select="pathway"/>
}
</xsl:template>


<xsl:template match="pathway">
"pathway":{
	"name":"<xsl:value-of select="@name"/>",
	"image":"<xsl:value-of select="@image"/>",
	"link":"<xsl:value-of select="@link"/>",
	"entries":[
		<xsl:for-each select="entry">
			<xsl:if test="position()&gt;1">,</xsl:if>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		],
	"relations":[
		<xsl:for-each select="relation">
			<xsl:if test="position()&gt;1">,</xsl:if>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		]
	}
</xsl:template>

<xsl:template match="entry">
{
"id":"<xsl:value-of select="@id"/>",
"name":"<xsl:value-of select="@name"/>",
"type":"<xsl:value-of select="@type"/>",
"reaction":"<xsl:value-of select="@reaction"/>",
"link":"<xsl:value-of select="@link"/>"
}
</xsl:template>

<xsl:template match="relation">
{
"entry1":"<xsl:value-of select="@entry1"/>",
"entry2":"<xsl:value-of select="@entry2"/>",
"type":"<xsl:value-of select="@type"/>"
}
</xsl:template>

</xsl:stylesheet>
