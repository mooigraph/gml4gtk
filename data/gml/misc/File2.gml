graph [
	comment "This is an example"
	directed 1
	node [
	id 1
	label "Node 1"
	]
	node [
	id 2
	label "Node 2"
	]
	node [
	id 3
	label "Node 3"
	]
	node [
	id 4
	label "Node 1"
	]
	node [
	id 5
	label "Node 2"
	]
	node [
	id 6
	label "Node 3"
	]
	node [
	id 7
	label "Node 1"
	]
	node [
	id 8
	label "Node 2"
	]
	node [
	id 9
	label "Node 3"
	]
	node [
	id 10
	label "Node 1"
	]
	node [
	id 11
	label "Node 2"
	]
	node [
	id 12
	label "Node 3"
	]
	edge [
	source 1
	target 2
	id 435
	label "Edge 1 - 2"
	]
	edge [
	source 1
	target 3
	label "Edge 1 - 3"
        id 436
	]
]
