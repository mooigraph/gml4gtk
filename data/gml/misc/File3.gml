

graph [

	comment "This is an example"

	directed 1

	node [

	id 1

	label "Node 1"

	]

	node [

	id 2

	label "Node 2"

	]

	node [

	id 3

	label "Node 3"

	]

	node [

	id 4

	label "Node 1"

	]

	node [

	id 5

	label "Node 2"

	]

	node [

	id 6

	label "Node 3"

	]

	node [

	id 7

	label "Node 1"

	]

	node [

	id 8

	label "Node 2"

	]

	node [

	id 9

	label "Node 3"

	]

	node [

	id 10

	label "Node 1"

	]

	node [

	id 11

	label "Node 2"

	]

	node [

	id 12

	label "Node 3"

	]

	node [

	id 13

	label "Node 1"

	]

	node [

	id 14

	label "Node 2"

	]

	node [

	id 15

	label "Node 3"

	]

	node [

	id 16

	label "Node 1"

	]

	node [

	id 17

	label "Node 2"

	]

	node [

	id 18

	label "Node 3"

	]

	node [

	id 19

	label "Node 1"

	]

	node [

	id 20

	label "Node 2"

	]

	node [

	id 21

	label "Node 3"

	]

	node [

	id 22

	label "Node 1"

	]

	node [

	id 23

	label "Node 2"

	]

	node [

	id 24

	label "Node 3"

	]

	edge [

	source 18

	target 22

	label "Edge 1 - 2"

	]

	edge [

	source 1

	target 13

	label "Edge 1 - 3"

	]

	edge [

	source 11

	target 2

	label "Edge 1 - 2"

	]

	edge [

	source 17

	target 18

	label "Edge 1 - 3"

	]

	edge [

	source 7

	target 24

	label "Edge 1 - 2"

	]

	edge [

	source 10

	target 23

	label "Edge 1 - 3"

	]

	edge [

	source 21

	target 12

	label "Edge 1 - 2"

	]

	edge [

	source 9

	target 13

	label "Edge 1 - 3"

	]

	edge [

	source 11

	target 12

	label "Edge 1 - 2"

	]

	edge [

	source 21

	target 6

	label "Edge 1 - 3"

	]

	edge [

	source 21

	target 22

	label "Edge 1 - 2"

	]

	edge [

	source 11

	target 23

	label "Edge 1 - 3"

	]

]

