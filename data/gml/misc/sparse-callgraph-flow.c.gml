graph
[
     node
     [
      id 139936821034160
      label "rewrite_branch()"
      graphics
      [
       w 420
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936821034160 target 139936821974352 ]
 edge [ source 139936821034160 target 139936822038032 ]
 edge [ source 139936821034160 target 139936821979280 ]
     node
     [
      id 139936821036848
      label "pseudo_truth_value()"
      graphics
      [
       w 540
       h 100
       fill "#009900"
      ]
     ]
     node
     [
      id 139936821039312
      label "bb_depends_on()"
      graphics
      [
       w 390
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936821039312 target 139936821032816 ]
     node
     [
      id 139936821045808
      label "bb_depends_on_phi()"
      graphics
      [
       w 510
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936821045808 target 139936821032816 ]
     node
     [
      id 139936821052528
      label "try_to_simplify_bb()"
      graphics
      [
       w 540
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936821052528 target 139936821984208 ]
 edge [ source 139936821052528 target 139936821984208 ]
 edge [ source 139936821052528 target 139936821036848 ]
 edge [ source 139936821052528 target 139936821039312 ]
 edge [ source 139936821052528 target 139936821045808 ]
 edge [ source 139936821052528 target 139936821034160 ]
 edge [ source 139936821052528 target 139936821034160 ]
 edge [ source 139936821052528 target 139936821116272 ]
     node
     [
      id 139936820964368
      label "bb_has_side_effects()"
      graphics
      [
       w 570
       h 100
       fill "#009900"
      ]
     ]
     node
     [
      id 139936820972208
      label "simplify_phi_branch()"
      graphics
      [
       w 570
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820972208 target 139936820964368 ]
 edge [ source 139936820972208 target 139936821052528 ]
     node
     [
      id 139936820974448
      label "simplify_branch_branch()"
      graphics
      [
       w 660
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820974448 target 139936820964368 ]
 edge [ source 139936820974448 target 139936821039312 ]
 edge [ source 139936820974448 target 139936821045808 ]
 edge [ source 139936820974448 target 139936821034160 ]
 edge [ source 139936820974448 target 139936821984208 ]
 edge [ source 139936820974448 target 139936821097680 ]
     node
     [
      id 139936820978704
      label "simplify_one_branch()"
      graphics
      [
       w 570
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820978704 target 139936820972208 ]
 edge [ source 139936820978704 target 139936820974448 ]
 edge [ source 139936820978704 target 139936820974448 ]
     node
     [
      id 139936820980272
      label "simplify_branch_nodes()"
      graphics
      [
       w 630
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820980272 target 139936820978704 ]
     node
     [
      id 139936820987216
      label "simplify_flow()"
      graphics
      [
       w 390
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820987216 target 139936820980272 ]
     node
     [
      id 139936820860240
      label "convert_instruction_target()"
      graphics
      [
       w 780
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820860240 target 139936822022576 ]
 edge [ source 139936820860240 target 139936821981520 ]
     node
     [
      id 139936820868080
      label "convert_load_instruction()"
      graphics
      [
       w 720
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820868080 target 139936820860240 ]
     node
     [
      id 139936820869872
      label "overlapping_memop()"
      graphics
      [
       w 510
       h 100
       fill "#009900"
      ]
     ]
     node
     [
      id 139936820875024
      label "dominates()"
      graphics
      [
       w 270
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820875024 target 139936820869872 ]
     node
     [
      id 139936820877264
      label "phisrc_in_bb()"
      graphics
      [
       w 360
       h 100
       fill "#009900"
      ]
     ]
     node
     [
      id 139936820883760
      label "find_dominating_parents()"
      graphics
      [
       w 690
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820883760 target 139936820875024 ]
 edge [ source 139936820883760 target 139936820883760 ]
 edge [ source 139936820883760 target 139936820877264 ]
 edge [ source 139936820883760 target 139936822035344 ]
 edge [ source 139936820883760 target 139936821099472 ]
 edge [ source 139936820883760 target 139936821979280 ]
 edge [ source 139936820883760 target 139936821979280 ]
 edge [ source 139936820883760 target 139936821253232 ]
 edge [ source 139936820883760 target 139936821979280 ]
     node
     [
      id 139936820767312
      label "rewrite_load_instruction()"
      graphics
      [
       w 720
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820767312 target 139936821118064 ]
 edge [ source 139936820767312 target 139936820868080 ]
 edge [ source 139936820767312 target 139936821116272 ]
     node
     [
      id 139936820778960
      label "find_dominating_stores()"
      graphics
      [
       w 660
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820778960 target 139936820875024 ]
 edge [ source 139936820778960 target 139936821930096 ]
 edge [ source 139936820778960 target 139936820868080 ]
 edge [ source 139936820778960 target 139936820883760 ]
 edge [ source 139936820778960 target 139936821023184 ]
 edge [ source 139936820778960 target 139936821101712 ]
 edge [ source 139936820778960 target 139936820868080 ]
 edge [ source 139936820778960 target 139936820767312 ]
     node
     [
      id 139936820789264
      label "kill_store()"
      graphics
      [
       w 300
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820789264 target 139936821116272 ]
     node
     [
      id 139936820790832
      label "kill_dead_stores()"
      graphics
      [
       w 480
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820790832 target 139936820789264 ]
 edge [ source 139936820790832 target 139936820790832 ]
     node
     [
      id 139936820644528
      label "kill_dominated_stores()"
      graphics
      [
       w 630
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820644528 target 139936820789264 ]
 edge [ source 139936820644528 target 139936820875024 ]
 edge [ source 139936820644528 target 139936820789264 ]
 edge [ source 139936820644528 target 139936821930096 ]
 edge [ source 139936820644528 target 139936820644528 ]
     node
     [
      id 139936820531664
      label "check_access()"
      graphics
      [
       w 360
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820531664 target 139936821718928 ]
 edge [ source 139936820531664 target 139936821930096 ]
     node
     [
      id 139936820533904
      label "simplify_one_symbol()"
      graphics
      [
       w 570
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820533904 target 139936821718928 ]
 edge [ source 139936820533904 target 139936821930096 ]
 edge [ source 139936820533904 target 139936820778960 ]
 edge [ source 139936820533904 target 139936820789264 ]
 edge [ source 139936820533904 target 139936820644528 ]
 edge [ source 139936820533904 target 139936820790832 ]
     node
     [
      id 139936820401040
      label "simplify_symbol_usage()"
      graphics
      [
       w 630
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820401040 target 139936820533904 ]
     node
     [
      id 139936820407088
      label "mark_bb_reachable()"
      graphics
      [
       w 510
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820407088 target 139936820407088 ]
     node
     [
      id 139936820413584
      label "kill_defs()"
      graphics
      [
       w 270
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820413584 target 139936820860240 ]
     node
     [
      id 139936820414928
      label "kill_bb()"
      graphics
      [
       w 210
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820414928 target 139936821118064 ]
 edge [ source 139936820414928 target 139936820413584 ]
 edge [ source 139936820414928 target 139936822038032 ]
 edge [ source 139936820414928 target 139936822038032 ]
     node
     [
      id 139936820301840
      label "kill_unreachable_bbs()"
      graphics
      [
       w 600
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820301840 target 139936820407088 ]
 edge [ source 139936820301840 target 139936820414928 ]
 edge [ source 139936820301840 target 139936821995408 ]
     node
     [
      id 139936820312144
      label "rewrite_parent_branch()"
      graphics
      [
       w 630
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820312144 target 139936821034160 ]
 edge [ source 139936820312144 target 139936821034160 ]
 edge [ source 139936820312144 target 139936822022576 ]
 edge [ source 139936820312144 target 139936821034160 ]
 edge [ source 139936820312144 target 139936822022576 ]
     node
     [
      id 139936820322224
      label "rewrite_branch_bb()"
      graphics
      [
       w 510
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820322224 target 139936820312144 ]
     node
     [
      id 139936820327376
      label "vrfy_bb_in_list()"
      graphics
      [
       w 450
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820327376 target 139936822022576 ]
     node
     [
      id 139936820203984
      label "vrfy_parents()"
      graphics
      [
       w 360
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820203984 target 139936820327376 ]
     node
     [
      id 139936820210256
      label "vrfy_children()"
      graphics
      [
       w 390
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820210256 target 139936822022576 ]
 edge [ source 139936820210256 target 139936820327376 ]
 edge [ source 139936820210256 target 139936820327376 ]
 edge [ source 139936820210256 target 139936820327376 ]
 edge [ source 139936820210256 target 139936820327376 ]
     node
     [
      id 139936820224368
      label "vrfy_bb_flow()"
      graphics
      [
       w 360
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820224368 target 139936820210256 ]
 edge [ source 139936820224368 target 139936820203984 ]
     node
     [
      id 139936820225488
      label "vrfy_flow()"
      graphics
      [
       w 270
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820225488 target 139936820224368 ]
 edge [ source 139936820225488 target 139936822022576 ]
     node
     [
      id 139936820233328
      label "pack_basic_blocks()"
      graphics
      [
       w 510
       h 100
       fill "#009900"
      ]
     ]
 edge [ source 139936820233328 target 139936820322224 ]
 edge [ source 139936820233328 target 139936820414928 ]
 edge [ source 139936820233328 target 139936821974352 ]
 edge [ source 139936820233328 target 139936822035344 ]
 edge [ source 139936820233328 target 139936821118064 ]
 edge [ source 139936820233328 target 139936822022576 ]
 edge [ source 139936820233328 target 139936821979280 ]
]
