# this is a comment line
# colors in nodes/edges using 'fill' 
graph [
  directed 1
  node [ id 0 label "n0" graphics [ fill "green" ] ]
  node [ id 1 label "n1" ]
  node [ id 2 label "n2" ]
  node [ id 3 label "n3" ]
  node [ id 4 label "n4" ]
  edge [ source 4 target 0 ]
  edge [ source 0 target 1 label "foo" ]
  edge [ source 1 target 2 graphics [ fill "#00f000" ] ]
  edge [ source 2 target 3 ]
  edge [ source 3 target 0 ]
  edge [ source 3 target 0 ]
]

