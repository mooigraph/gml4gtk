graph [
	comment "This is an example"
	directed 1
	node [
	id 1
	label "Node 1"
	graphics [
		x 20.0
		y 40.0
		]
	]
	node [
	id 2
	label "Node 2"
	]
	node [
	id 3
	label "Node 3"
	]
	edge [
	source 1
	target 2
	label "Edge 1 - 2"
	]
	edge [
	source 1
	target 3
	label "Edge 1 - 3"
	]
]
