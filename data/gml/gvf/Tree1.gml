graph [
   directed 1
   node [
      id 1
      label "N_0_0"
   ]
   node [
      id 2
      label "N_1_0"
   ]
   node [
      id 3
      label "N_1_1"
   ]
   node [
      id 4
      label "N_2_0"
   ]
   node [
      id 5
      label "N_2_1"
   ]
   node [
      id 6
      label "N_1_2"
   ]
   node [
      id 7
      label "N_2_2"
   ]
   node [
      id 8
      label "N_2_3"
   ]
   node [
      id 9
      label "N_3_0"
   ]
   node [
      id 10
      label "N_3_1"
   ]
   edge [
      source 1
      target 2
   ]
   edge [
      source 1
      target 3
   ]
   edge [
      source 3
      target 4
   ]
   edge [
      source 3
      target 5
   ]
   edge [
      source 1
      target 6
   ]
   edge [
      source 6
      target 7
   ]
   edge [
      source 6
      target 8
   ]
   edge [
      source 8
      target 9
   ]
   edge [
      source 8
      target 10
   ]
]
