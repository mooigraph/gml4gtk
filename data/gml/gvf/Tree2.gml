graph [
   directed 1
   node [
      id 1
      label "N_0_0"
   ]
   node [
      id 2
      label "N_1_0"
   ]
   node [
      id 3
      label "N_1_1"
   ]
   node [
      id 4
      label "N_2_0"
   ]
   node [
      id 5
      label "N_3_0"
   ]
   node [
      id 6
      label "N_2_1"
   ]
   node [
      id 7
      label "N_1_2"
   ]
   node [
      id 8
      label "N_2_2"
   ]
   node [
      id 9
      label "N_3_1"
   ]
   node [
      id 10
      label "N_4_0"
   ]
   node [
      id 11
      label "N_4_1"
   ]
   node [
      id 12
      label "N_4_2"
   ]
   node [
      id 13
      label "N_5_0"
   ]
   node [
      id 14
      label "N_5_1"
   ]
   node [
      id 15
      label "N_5_2"
   ]
   node [
      id 16
      label "N_5_3"
   ]
   node [
      id 17
      label "N_3_2"
   ]
   node [
      id 18
      label "N_4_3"
   ]
   node [
      id 19
      label "N_4_4"
   ]
   node [
      id 20
      label "N_5_4"
   ]
   node [
      id 21
      label "N_5_5"
   ]
   node [
      id 22
      label "N_5_6"
   ]
   node [
      id 23
      label "N_4_5"
   ]
   node [
      id 24
      label "N_5_7"
   ]
   node [
      id 25
      label "N_2_3"
   ]
   edge [
      source 1
      target 2
   ]
   edge [
      source 1
      target 3
   ]
   edge [
      source 3
      target 4
   ]
   edge [
      source 4
      target 5
   ]
   edge [
      source 3
      target 6
   ]
   edge [
      source 1
      target 7
   ]
   edge [
      source 7
      target 8
   ]
   edge [
      source 8
      target 9
   ]
   edge [
      source 9
      target 10
   ]
   edge [
      source 9
      target 11
   ]
   edge [
      source 9
      target 12
   ]
   edge [
      source 12
      target 13
   ]
   edge [
      source 12
      target 14
   ]
   edge [
      source 12
      target 15
   ]
   edge [
      source 12
      target 16
   ]
   edge [
      source 8
      target 17
   ]
   edge [
      source 17
      target 18
   ]
   edge [
      source 17
      target 19
   ]
   edge [
      source 19
      target 20
   ]
   edge [
      source 19
      target 21
   ]
   edge [
      source 19
      target 22
   ]
   edge [
      source 17
      target 23
   ]
   edge [
      source 23
      target 24
   ]
   edge [
      source 7
      target 25
   ]
]
