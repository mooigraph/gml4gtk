# gmlt
  
Small console test program for GML (Graph Markup Language) graph language directed graph layout
  
To compile:  
./autogen.sh  
./configure  
make  
cd src  
./gmlt input.gml < input.gml  
  
GNU / Linux is user-friendly. It's just particular who its friends are :)  
  
SPDX-License-Identifier: GPL-3.0+  
License-Filename: LICENSE  
  
