
/*
 *  Copright 2021
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

/* parse the ci data generated with gcc -fcallgraph-info option
 * this does only handle the gcc ci file part which is a minimal language
 * and this program has nothing to do with that other tool
 * the gcc-12 ci routines need update to better handle indirect function calls
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>

#include "splay-tree.h"
#include "main.h"
#include "hier.h"
#include "uniqnode.h"
#include "uniqstr.h"
#include "ci.h"
#include "cius.h"
#include "ciun.h"
#include "ci.yy.h"
#include "ci.tab.h"
#include "dpmem.h"
#include "dot.tab.h"

/* like yydebug, set via lexer init */
int cidebug = 0;

/* current lexed token */
static int citoken = 0;

/* set at parse error */
static int cierror = 0;

/* nodes list */
static struct cin *vnl = NULL;
static struct cin *vnlend = NULL;

/* edges list */
static struct cie *vel = NULL;
static struct cie *velend = NULL;

/* graph title */
static char *graphtitle = NULL;

/* current file name */
static char *curfname = NULL;

/* uniq node number */
static int cinn = 0;

/* uniq edge number */
static int cien = 0;

/* clear edge list */
static void vel_clear(void)
{
   struct cie *pvnl = NULL;
   struct cie *pvnlnext = NULL;

   pvnl = vel;

   while (pvnl) {
      pvnlnext = pvnl->next;
      pvnl = dp_free(pvnl);
      if (pvnl) {
      }
      pvnl = pvnlnext;
   }

   return;
}

/* clear node list */
static void vnl_clear(void)
{
   struct cin *pvnl = NULL;
   struct cin *pvnlnext = NULL;

   pvnl = vnl;

   while (pvnl) {
      pvnlnext = pvnl->next;
      pvnl = dp_free(pvnl);
      if (pvnl) {
      }
      pvnl = pvnlnext;
   }

   return;
}

/* clear all at parse error */
static void ciparse_clear(void)
{

   /* clear edge list */
   vel_clear();

   /* clear node list */
   vnl_clear();

   /* in ci.l */
   ci_lex_clear();

   /* node db */
   clear_ciuniqnode();

   /* uniq strings db */
   ci_clear_uniqstr();

   /* no debug */
   cidebug = 0;

   /* current lexed token */
   citoken = 0;

   /* set at parse error */
   cierror = 0;

   /* nodes list */
   vnl = NULL;
   vnlend = NULL;

   /* edges list */
   vel = NULL;
   velend = NULL;

   /* global graph title */
   graphtitle = NULL;

   /* current filename */
   curfname = NULL;

   /* uniq node number */
   cinn = 0;

   /* uniq edge number */
   cien = 0;

   return;
}

/* handle graph title statement */
static void cigraphtitle(void)
{
   citoken = cilex();

   if (citoken != CI_COLON) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"title\" at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
      return;
   }

   citoken = cilex();

   if (citoken != CI_STRING) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected string after \"title:\" at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
      return;
   }

   graphtitle = cilaststring;

   return;
}

/* handle a node statement */
static void cinode(void)
{
   int shape = 0;
   char *title = NULL;
   char *label = NULL;
   struct cin *newnode = NULL;

   citoken = cilex();

   if (citoken != CI_COLON) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"node\" at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
      return;
   }

   citoken = cilex();

   if (citoken != CI_BO) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected '{' after \"node:\" at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
      return;
   }

   for (;;) {
      if (cierror) {
	 break;
      }

      citoken = cilex();

      if (citoken == EOF) {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): unexpected end-of-file in \"node:\" at line %d in file %s\n", __func__, cilineno, curfname);
	 cierror = 1;
	 break;
      }

      /* an brace close at end of node statement */
      if (citoken == CI_BC) {
	 break;
      }

      if (citoken == CI_TITLE) {
	 citoken = cilex();

	 if (citoken != CI_COLON) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"title\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 citoken = cilex();

	 if (citoken != CI_STRING) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected string after \"title:\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 title = cilaststring;
	 cilaststring = NULL;

      } else if (citoken == CI_LABEL) {
	 citoken = cilex();

	 if (citoken != CI_COLON) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"label\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }
	 citoken = cilex();

	 if (citoken != CI_STRING) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected string after \"label:\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 label = cilaststring;
	 cilaststring = NULL;

      } else if (citoken == CI_SHAPE) {
	 citoken = cilex();

	 if (citoken != CI_COLON) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"shape\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 citoken = cilex();

	 if (citoken != CI_ELLIPSE) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected ellipse after \"shape:\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 shape = 1;

      } else {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): unknown node statement `%s' at line %d in file %s\n", __func__, citext, cilineno, curfname);
	 cierror = 1;
	 break;
      }

   }

   if (cierror) {
      return;
   }

   if (title == NULL) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): node has no \"title:\" statement at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
   }

   /* when no label specified, use the node name */
   if (label == NULL) {
      label = title;
   }

   newnode = dp_calloc(1, sizeof(struct cin));

   if (newnode == NULL) {
      cierror = 1;
      return;
   }

   /* uniq node number starts at 1 */
   cinn++;

   newnode->nr = cinn;
   newnode->name = title;
   newnode->label = label;
   newnode->shape = shape;

   if (vnl == NULL) {
      vnl = newnode;
      vnlend = newnode;
   } else {
      vnlend->next = newnode;
      vnlend = newnode;
   }

   /* add node to db */
   ciuniqnode_add(newnode);

   return;
}

/* handle a edge statement */
static void ciedge(void)
{
   struct cie *newedge = NULL;
   char *sn = NULL;
   char *tn = NULL;
   char *label = NULL;

   citoken = cilex();

   if (citoken != CI_COLON) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"edge\" at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
      return;
   }

   citoken = cilex();

   if (citoken != CI_BO) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected '{' after \"edge:\" at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
      return;
   }

   for (;;) {
      if (cierror) {
	 break;
      }

      citoken = cilex();

      if (citoken == EOF) {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): unexpected end-of-file in \"edge:\" at line %d in file %s\n", __func__, cilineno, curfname);
	 cierror = 1;
	 break;
      }

      /* an brace close at end of node statement */
      if (citoken == CI_BC) {
	 break;
      }

      if (citoken == CI_SOURCENAME) {
	 citoken = cilex();

	 if (citoken != CI_COLON) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"sourcename\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 citoken = cilex();

	 if (citoken != CI_STRING) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected string after \"sourcename:\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 sn = cilaststring;
	 cilaststring = NULL;

      } else if (citoken == CI_TARGETNAME) {
	 citoken = cilex();

	 if (citoken != CI_COLON) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"targetname\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 citoken = cilex();

	 if (citoken != CI_STRING) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected string after \"targetname:\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 tn = cilaststring;
	 cilaststring = NULL;

      } else if (citoken == CI_LABEL) {
	 citoken = cilex();

	 if (citoken != CI_COLON) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"label\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 citoken = cilex();

	 if (citoken != CI_STRING) {
	    memset(parsermessage, 0, 256);
	    snprintf(parsermessage, (256 - 1), "ci %s(): expected string after \"label:\" at line %d in file %s\n", __func__, cilineno, curfname);
	    cierror = 1;
	    break;
	 }

	 label = cilaststring;
	 cilaststring = NULL;
      } else {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): unknown edge statement `%s' at line %d in file %s\n", __func__, citext, cilineno, curfname);
	 cierror = 1;
	 break;
      }

   }

   if (cierror) {
      return;
   }

   if (sn == NULL) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): edge has no \"sourcenode:\" statement at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
   }

   if (tn == NULL) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): edge has no \"targetnode:\" statement at line %d in file %s\n", __func__, cilineno, curfname);
      cierror = 1;
   }

   if (cierror) {
      return;
   }

   newedge = dp_calloc(1, sizeof(struct cie));

   if (newedge == NULL) {
      cierror = 1;
      return;
   }

   /* uniq edge number */
   cien++;

   newedge->nr = cien;

   newedge->fns = sn;		/* uniq name from-node */
   newedge->fromnode = ciuniqnode(sn);
   newedge->tns = tn;		/* uniq name to-node */
   newedge->tonode = ciuniqnode(tn);
   newedge->label = label;

   if (vel == NULL) {
      vel = newedge;
      velend = newedge;
   } else {
      velend->next = newedge;
      velend = newedge;
   }

   return;
}

/* check if all nodes in edges are defined */
static void chkedges(void)
{
   struct cie *pvel = NULL;
   struct cin *pn = NULL;

   pvel = vel;

   while (pvel) {
      pn = ciuniqnode(pvel->fns);
      if (pn == NULL) {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): sourcenode `%s' in edge not defined in file %s\n", __func__, pvel->fns, curfname);
	 cierror = 1;
	 break;
      }
      if (pvel->fromnode == NULL) {
	 pvel->fromnode = pn;
      }
      /* set node number in edge */
      pvel->fnn = pn->nr;
      pn = ciuniqnode(pvel->tns);
      if (pn == NULL) {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): targetnode `%s' in edge not defined in file %s\n", __func__, pvel->tns, curfname);
	 cierror = 1;
	 break;
      }
      if (pvel->tonode == NULL) {
	 pvel->tonode = pn;
      }
      /* set node number in edge */
      pvel->tnn = pn->nr;
      pvel = pvel->next;
   }

   return;
}

/* copy checked edges and nodes now */
static void copyall(struct gml_graph *g)
{
   struct gml_node *fnn = NULL;
   struct gml_node *tnn = NULL;
   struct cin *pvnl = NULL;
   struct cie *pvel = NULL;
   int nr = 0;

   pvnl = vnl;

   while (pvnl) {
      /* uniq node number starting at 1 */
      maingraph->nodenum++;
      nr = maingraph->nodenum;
      /* in ci all nodes are located in rootgraph
       * use -1 for foundid then gml id check is not applied.
       */
      add_new_node(g, maingraph, nr, /* pvnl->nr */ (-1), uniqstr(pvnl->name), uniqstr(pvnl->label), /* ncolor */ 0x00ffffff, /* nbcolor */ 0,	/* rlabel */
		   NULL, /* hlabel */ NULL, /* fontcolor */ 0, /* ishtml */ 0);
      pvnl->finalnr = nr;
      pvnl = pvnl->next;
   }

   pvel = vel;

   while (pvel) {
      if (pvel->fromnode) {
	 fnn = uniqnode(g, pvel->fromnode->finalnr);
      } else {
	 fnn = NULL;
      }
      if (pvel->tonode) {
	 tnn = uniqnode(g, pvel->tonode->finalnr);
      } else {
	 tnn = NULL;
      }
      if (fnn && tnn) {
	 if (fnn == tnn) {
	    /* add selfedge count at this node */
	    fnn->nselfedges++;
	 } else {
	    add_new_edge(g, maingraph, /* foundsource */ fnn->nr, /* foundtarget */ tnn->nr,
			 uniqstr(pvel->label),
			 /* ecolor */ 0, /* style */ 0, /* fcompass */ NULL, /* tcompass */ NULL,
			 /* constraint */ 1, /* ishtml */ 0);
	 }
      } else {
	 printf("ci %s(): missing nodes in edge between nodes ", __func__);
	 if (pvel->fns) {
	    printf("\"%s\" ", pvel->fns);
	 }
	 if (pvel->tns) {
	    printf("\"%s\" ", pvel->tns);
	 }
	 printf("\n");
      }
      pvel = pvel->next;
   }
   return;
}

/* */
int ciparse(struct gml_graph *g, gzFile f, char *fname, char *argv0)
{

   /* no debug output by default. */
   yydebug = 0;

   /* optional turn on debug */
   if (argv0) {
      if (strcmp(argv0, "gml4gtkd") == 0) {
	 yydebug = 1;
      }
   }

   curfname = fname;

   /* uniq node number */
   cinn = 0;

   /* uniq edge number */
   cien = 0;

   /* no errors */
   cierror = 0;

   /* in ci.l */
   ci_lex_init(f, /* debugflag */ yydebug);

   /* expect "graph" as first token in ci graph */
   citoken = cilex();

   if (citoken != CI_GRAPH) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected \"graph\" at start of graph in file %s\n", __func__, fname);
      ciparse_clear();
      return (1);
   }

   /* expect : */
   citoken = cilex();

   if (citoken != CI_COLON) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected ':' after \"graph\" at start of graph in file %s\n", __func__, fname);
      ciparse_clear();
      return (1);
   }

   /* expect brace open */
   citoken = cilex();

   if (citoken != CI_BO) {
      memset(parsermessage, 0, 256);
      snprintf(parsermessage, (256 - 1), "ci %s(): expected '{' after \"graph:\" at start of graph in file %s\n", __func__, fname);
      ciparse_clear();
      return (1);
   }

   /* parse until brace close */

   for (;;) {

      /* check if there was parse error */
      if (cierror) {
	 break;
      }

      citoken = cilex();

      if (citoken == EOF) {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): unexpected end-of-file at line %d in file %s\n", __func__, cilineno, fname);
	 cierror = 1;
	 break;
      }

      /* at brace close end of graph */
      if (citoken == CI_BC) {
	 break;
      }

      /* at top level in graph is in gcc data
       * title: "string"
       * node: {...}
       * edge: {...}
       */
      if (citoken == CI_TITLE) {
	 cigraphtitle();
      } else if (citoken == CI_NODE) {
	 cinode();
      } else if (citoken == CI_EDGE) {
	 ciedge();
      } else {
	 memset(parsermessage, 0, 256);
	 snprintf(parsermessage, (256 - 1), "ci %s(): unknown graph statement `%s' at line %d in file %s\n", __func__, citext, cilineno, fname);
	 cierror = 1;
	 break;
      }

   }

   /* check if there was parse error */
   if (cierror) {
      ciparse_clear();
      return (1);
   }

   /* if graph has no nodes, done with oke status */
   if (vnl == NULL) {
      ciparse_clear();
      return (0);
   }

   /* check if all nodes in edges are defined */
   chkedges();

   /* check if there was parse error */
   if (cierror) {
      ciparse_clear();
      return (1);
   }

   /* copy checked edges and nodes now */
   copyall(g);

   ciparse_clear();

   return (0);
}

/* end */
