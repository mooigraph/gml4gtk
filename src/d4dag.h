
/*
 *  Junio H Hamano gitster@pobox.com
 *  rewrite of the GNU GPL javascript sugiyama Copyright Simon Speich 2013, 2021
 *  The GNU GPL Free version 3 javascript is at https://github.com/speich/dGraph
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef D4DAG_H
#define D4DAG_H 1

#ifdef __cplusplus
extern "C" {
#endif

/**
 * returns version number as int
 */
extern int d4d_version (void);

/**
 * init with mandatory malloc/free wrapper
 * return
 * -1 at error
 *  0 oke
 */
extern int d4d_init(void *(*mallocer)(unsigned int n), void (*freeer)(void *ptr));

/**
 * free all memory
 */
extern int d4d_deinit (void);

#ifdef __cplusplus
}
#endif

#endif

/* end. */
