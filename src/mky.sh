#!/bin/sh -x

exit 0

rm dot.tab.c
rm dot.tab.h
rm dot.output
rm dot.xml
echo "generating parser"

# or use bison --skeleton=./glr.c -d --graph=dot.gv --language=c  -x dot.y
# this uses the local bison.m4 and glr.c which does not have the special copyright exception and is GNU GPL Free software
# some other tool used a GNU GPL parser code and removed the GNU GPL copyright statement.

echo "THE PARSER SOURCE CODE DOES NOT HAVE THE GNU BISON SPECIAL Free Software Foundation EXCEPTION AND IS GNU GPL FREE SOFTWARE VERSION 3+"

bison -d --graph=dot.gv --language=c  -x dot.y

echo "generating dot html parser"
bison -d --graph=dphl.gv dphl.y

echo "debian 10 has bison version 3.3.2"
echo "debian 10 has bison version 3.7.5"
echo "fedora 34 has bison version 3.7.4"


