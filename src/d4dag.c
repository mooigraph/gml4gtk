
/*

 *  Junio H Hamano gitster@pobox.com
 *  rewrite of the GNU GPL javascript sugiyama Copyright Simon Speich 2013, 2021
 *  The GNU GPL Free version 3 javascript is at https://github.com/speich/dGraph
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * This uses modified splay tree algorithm from GNU GCC compiler 12 in directory liberty
 * This modified version added new routines and verified correct with 400+ million nodes.
 * A splay-tree datatype.
 * Copyright (C) 1998-2021 Free Software Foundation, Inc.
 * Contributed by Mark Mitchell (mark@markmitchell.com).
 * This file is part of GNU CC.
 *
 * GNU CC is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * GNU CC is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNU CC; see the file COPYING.  If not, write to
 * the Free Software Foundation, 51 Franklin Street - Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * For an easily readable description of splay-trees, see:
 *
 *      Lewis, Harry R. and Denenberg, Larry.  Data Structures and Their
 *      Algorithms.  Harper-Collins, Inc.  1991.
 *
 * The splay algorithm is a fast lookup based on the observation
 * when a item is searched often the next search is close to the
 * earlier item and the tree is rearranged for this at every lookup
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

/**
 * all routines have prefix d4d_
 * all static routines and vars have prefix d4d___
 * the prefix d4d_ has no special meaning but hopefully
 * does not problems because of limited C namespace
 */

/* only in debug version linkage needed to printf */
#if D4D_DEBUG
#include <stdio.h>
#define print_debug(str, ...) printf(str" - %s:%u\n", ##__VA_ARGS__, __FILE__, __LINE__);
#endif

/* datatypoes used are
 * int, as signed 32bits integer value
 * unsigned int, as 32bits integer value
 * char, as signed 8bits integer value
 * unsigned char, as 8bits integer value
 * unsigned long long int, as 64 bits unsigned integer value which can hold a 64 bits pointer
 */

/**
 * set here version number as int
 */
#define D4DAG_VERSION 10

/* prefix local routines */
#define CN(x)   d4dag___ ## x

/* defs and prototypes of routines for user program */
#include "d4dag.h"

/* mallocer/freeer */
typedef void *(*malloc_fn)(unsigned int n);
typedef void (*free_fn)(void *ptr);

/* this should be at least 64bits */
#ifndef uintptr_t
typedef unsigned long long int uintptr_t;
#endif

/* Use typedefs for the key and data types to facilitate changing
   these types, if necessary.  These types should be sufficiently wide
   that any pointer or scalar can be cast to these types, and then
   cast back, without loss of precision.  */
typedef uintptr_t splay_tree_key;	/* 64bits unsigned int */
typedef uintptr_t splay_tree_value;

/* Forward declaration for a node in the tree.  */
typedef struct splay_tree_node_s *splay_tree_node;

/* The type of a function which compares two splay-tree keys.  The
   function should return values as for qsort.  */
typedef int (*splay_tree_compare_fn)(splay_tree_key, splay_tree_key);

/* The type of a function used to deallocate any resources associated
   with the key.  If you provide this function, the splay tree
   will take the ownership of the memory of the splay_tree_key arg
   of splay_tree_insert.  This function is called to release the keys
   present in the tree when calling splay_tree_delete or splay_tree_remove.
   If splay_tree_insert is called with a key equal to a key already
   present in the tree, the old key and old value will be released.  */
typedef void (*splay_tree_delete_key_fn)(splay_tree_key);

/* The type of a function used to deallocate any resources associated
   with the value.  If you provide this function, the memory of the
   splay_tree_value arg of splay_tree_insert is managed similarly to
   the splay_tree_key memory: see splay_tree_delete_key_fn.  */
typedef void (*splay_tree_delete_value_fn)(splay_tree_value);

/* The type of a function used to iterate over the tree.  */
typedef int (*splay_tree_foreach_fn)(splay_tree_node, void *);

/* The type of a function used to allocate memory for tree root and
   node structures.  The first argument is the number of bytes needed;
   the second is a data pointer the splay tree functions pass through
   to the allocator.  This function must never return zero.  */
/* old typedef void *(*splay_tree_allocate_fn)(int, void *); */
typedef void *(*splay_tree_allocate_fn)(unsigned int, void *);

/* The type of a function used to free memory allocated using the
   corresponding splay_tree_allocate_fn.  The first argument is the
   memory to be freed; the latter is a data pointer the splay tree
   functions pass through to the freer.  */
typedef void (*splay_tree_deallocate_fn)(void *, void *);

/* The nodes in the splay tree.  */
struct splay_tree_node_s {
	/* The key.  */
	splay_tree_key key;

	/* The value.  */
	splay_tree_value value;

	/* The left and right children, respectively.  */
	splay_tree_node left;
	splay_tree_node right;
};

/* The splay tree itself.  */
struct splay_tree_s {
	/* The root of the tree.  */
	splay_tree_node root;

	/* The comparision function.  */
	splay_tree_compare_fn comp;

	/* The deallocate-key function.  NULL if no cleanup is necessary.  */
	splay_tree_delete_key_fn delete_key;

	/* The deallocate-value function.  NULL if no cleanup is necessary.  */
	splay_tree_delete_value_fn delete_value;

	/* Node allocate function.  Takes allocate_data as a parameter. */
	splay_tree_allocate_fn allocate;

	/* Free function for nodes and trees.  Takes allocate_data as a parameter.  */
	splay_tree_deallocate_fn deallocate;

	/* Parameter for allocate/free functions.  */
	void *allocate_data;
};

typedef struct splay_tree_s *splay_tree;

/* toplevel control struct */
struct d4d__maing {
	/* wrappers for malloc/free and all malloc's will be below 4Gb at once */
	malloc_fn d4d__malloc;
	free_fn d4d__free;
};

/* splay from gcc compiler */
static void splay_tree_delete_helper(splay_tree sp, splay_tree_node node);
static void rotate_left(splay_tree_node * pp, splay_tree_node p, splay_tree_node n);
static void rotate_right(splay_tree_node * pp, splay_tree_node p, splay_tree_node n);
static void splay_tree_splay(splay_tree sp, splay_tree_key key);
static void *splay_tree_xmalloc_allocate(unsigned int size, void *data);
static void splay_tree_xmalloc_deallocate(void *object, void *data);
static splay_tree splay_tree_new(splay_tree_compare_fn compare_fn, splay_tree_delete_key_fn delete_key_fn, splay_tree_delete_value_fn delete_value_fn);
static void CN(splay_tree_delete) (splay_tree sp);
static splay_tree_node CN(splay_tree_insert) (splay_tree sp, splay_tree_key key, splay_tree_value value);
static void CN(splay_tree_remove) (splay_tree sp, splay_tree_key key);
static splay_tree_node splay_tree_lookup(splay_tree sp, splay_tree_key key);
static splay_tree_node splay_tree_max(splay_tree sp);
static splay_tree_node CN(splay_tree_min) (splay_tree sp);
static splay_tree_node CN(splay_tree_predecessor) (splay_tree sp, splay_tree_key key);
static splay_tree_node splay_tree_successor(splay_tree sp, splay_tree_key key);
static int splay_tree_foreach(splay_tree sp, splay_tree_foreach_fn fn, void *data);
static int splay_tree_compare_ints(splay_tree_key k1, splay_tree_key k2);
static int splay_tree_compare_pointers(splay_tree_key k1, splay_tree_key k2);
static int splay_tree_compare_strings(splay_tree_key k1, splay_tree_key k2);
static void splay_tree_delete_pointers(splay_tree_value value);
static splay_tree splay_tree_new_typed_alloc(splay_tree_compare_fn compare_fn, splay_tree_delete_key_fn delete_key_fn, splay_tree_delete_value_fn delete_value_fn, splay_tree_allocate_fn tree_allocate_fn,
					     splay_tree_allocate_fn node_allocate_fn, splay_tree_deallocate_fn deallocate_fn, void *allocate_data);
static splay_tree splay_tree_new_with_allocator(splay_tree_compare_fn compare_fn, splay_tree_delete_key_fn delete_key_fn, splay_tree_delete_value_fn delete_value_fn, splay_tree_allocate_fn allocate_fn,
						splay_tree_deallocate_fn deallocate_fn, void *allocate_data);

/* main control */
static struct d4d__maing *d4d__main = (struct d4d__maing *)0;

/* forward decl. */
static void d4d__memzero(void *ptr, unsigned int n);
/**
 * returns version number as int
 */
int d4d_version(void)
{
	return (D4DAG_VERSION);
}

/**
 *
 * returns:
 *  0 if oke
 * -1 if missing malloc/free pointer
 * -2 if malloc failed
 * For embedded devices this source has no linkage to libraries
 * which means here pointer to malloc/free must be supplied.
 * when malloc returns zero then these routines crashes
 */
int d4d_init(void *(*mallocer)(unsigned int n), void (*freeer)(void *ptr))
{
	/* char tenbytes[10];
	 * sizeof(0, tenbytes) is 10 when compiled as c++ and 8 when compiled as c
	 */
	/* malloc/free pointers must be specified */
	if (!mallocer) {
		return (-1);
	}
	if (!freeer) {
		return (-1);
	}
	d4d__main = (struct d4d__maing *)(*mallocer) ((unsigned int)sizeof(struct d4d__maing));
	if (!d4d__main) {
		return (-2);
	}
	/* set pointers to malloc/free in toplevel control */
	d4d__memzero(d4d__main, sizeof(struct d4d__maing));
	d4d__main->d4d__malloc = mallocer;
	d4d__main->d4d__free = freeer;

	/* small splay tree test */
	{
		splay_tree spt = (splay_tree) 0;
		unsigned long long int n = 0;	/* 64bits */
		splay_tree_node spn = (splay_tree_node) 0;

		spt = splay_tree_new(splay_tree_compare_ints /* compare_fn */ ,
				     (splay_tree_delete_key_fn) 0 /* delete_key_fn */ ,
				     (splay_tree_delete_value_fn) 0	/* delete_value_fn */
		    );

		/* create small splay tree does not use realloc() */
		for (n = 0; n < 10; n++) {
			spn /* splay_tree_node */  =
			    CN(splay_tree_insert) ((splay_tree) spt, (splay_tree_key) n, (splay_tree_value) 0);
			if (!spn) {	/* shouldnothappen */
			}
		}

		CN(splay_tree_delete) ((splay_tree) spt);
	}

	return (0);
}

/**
 *
 */
int d4d_deinit(void)
{
	/* check if active */
	if (!d4d__main) {
		return (0);
	}
	/* free structs */
	/* free control */
	d4d__main->d4d__free(d4d__main);
	d4d__main = (struct d4d__maing *)0;
	return (0);
}

/* like memset(ptr,0,n)
 * note that intel icc compiler does inline this and gcc not
 */
static void d4d__memzero(void *ptr, unsigned int n)
{
	unsigned char *p = (unsigned char *)0;
	p = (unsigned char *)ptr;
	while (n) {
		*p = 0;
		p++;
		n--;
	}
	return;
}

/* zz3 marker */

/* Deallocate NODE (a member of SP), and all its sub-trees.  */

static void splay_tree_delete_helper(splay_tree sp, splay_tree_node node)
{
	splay_tree_node pending = (splay_tree_node) 0;
	splay_tree_node active = (splay_tree_node) 0;

	if (!node) {
		return;
	}

	if (sp->delete_key) {
		(*sp->delete_key) (node->key);
	}

	if (sp->delete_value) {
		(*sp->delete_value) (node->value);
	}

	/* We use the "key" field to hold the "next" pointer.  */
	node->key = (splay_tree_key) pending;
	pending = (splay_tree_node) node;

	/* Now, keep processing the pending list until there aren't any
	   more.  This is a little more complicated than just recursing, but
	   it doesn't toast the stack for large trees.  */

	while (pending) {
		active = pending;
		pending = 0;
		while (active) {
			splay_tree_node temp = (splay_tree_node) 0;

			/* active points to a node which has its key and value
			   deallocated, we just need to process left and right.  */

			if (active->left) {
				if (sp->delete_key) {
					(*sp->delete_key) (active->left->key);
				}
				if (sp->delete_value) {
					(*sp->delete_value)
					    (active->left->value);
				}
				active->left->key = (splay_tree_key) pending;
				pending = (splay_tree_node) (active->left);
			}
			if (active->right) {
				if (sp->delete_key) {
					(*sp->delete_key) (active->right->key);
				}
				if (sp->delete_value) {
					(*sp->delete_value) (active->right->value);
				}
				active->right->key = (splay_tree_key) pending;
				pending = (splay_tree_node) (active->right);
			}

			temp = active;
			active = (splay_tree_node) (temp->key);
			(*sp->deallocate) ((char *)temp, sp->allocate_data);
		}
	}

	return;
}

/* Rotate the edge joining the left child N with its parent P.  PP is the
   grandparents' pointer to P.  */

static void rotate_left(splay_tree_node * pp, splay_tree_node p, splay_tree_node n)
{
	splay_tree_node tmp = (splay_tree_node) 0;
	tmp = n->right;
	n->right = p;
	p->left = tmp;
	*pp = n;
	return;
}

/* Rotate the edge joining the right child N with its parent P.  PP is the
   grandparents' pointer to P.  */

static void rotate_right(splay_tree_node * pp, splay_tree_node p, splay_tree_node n)
{
	splay_tree_node tmp = (splay_tree_node) 0;
	tmp = n->left;
	n->left = p;
	p->right = tmp;
	*pp = n;
	return;
}

/* Bottom up splay of key.  */

static void splay_tree_splay(splay_tree sp, splay_tree_key key)
{
	int cmp1 = 0;
	int cmp2 = 0;;
	splay_tree_node n = (splay_tree_node) 0;
	splay_tree_node c = (splay_tree_node) 0;

	if (!sp->root) {
		/* no data */
		return;
	}

	do {

		n = sp->root;
		cmp1 = (*sp->comp) (key, n->key);

		/* Found.  */
		if (cmp1 == 0) {
			return;
		}

		/* Left or right?  If no child, then we're done.  */
		if (cmp1 < 0) {
			c = n->left;
		} else {
			c = n->right;
		}

		if (!c) {
			return;
		}

		/* Next one left or right?  If found or no child, we're done
		   after one rotation.  */
		cmp2 = (*sp->comp) (key, c->key);
		if ((cmp2 == 0) || ((cmp2 < 0) && (!c->left)) || ((cmp2 > 0) && (!c->right))) {
			if (cmp1 < 0) {
				rotate_left(&sp->root, n, c);
			} else {
				rotate_right(&sp->root, n, c);
			}
			return;
		}

		/* Now we have the four cases of double-rotation.  */
		if ((cmp1 < 0) && (cmp2 < 0)) {
			rotate_left(&n->left, c, c->left);
			rotate_left(&sp->root, n, n->left);
		} else if ((cmp1 > 0) && (cmp2 > 0)) {
			rotate_right(&n->right, c, c->right);
			rotate_right(&sp->root, n, n->right);
		} else if ((cmp1 < 0) && (cmp2 > 0)) {
			rotate_right(&n->left, c, c->right);
			rotate_left(&sp->root, n, n->left);
		} else if ((cmp1 > 0) && (cmp2 < 0)) {
			rotate_left(&n->right, c, c->left);
			rotate_right(&sp->root, n, n->right);
		} else {
			/* nop shouldnothappen */
		}

	}
	while (1);

	return;
}

/* An allocator and deallocator based on xmalloc.  */
static void *splay_tree_xmalloc_allocate(unsigned int size, void *data)
{
	void *ret = (void *)0;
	if (data) {		/* not used */
	}
	ret = (void *)d4d__main->d4d__malloc(size);
	d4d__memzero(ret, size);
	return (ret);
}

static void splay_tree_xmalloc_deallocate(void *object, void *data)
{
	if (object) {
		d4d__main->d4d__free(object);
	}
	if (data) {		/* not used */
	}
	return;
}

/* Allocate a new splay tree, using COMPARE_FN to compare nodes,
   DELETE_KEY_FN to deallocate keys, and DELETE_VALUE_FN to deallocate
   values.  Use xmalloc to allocate the splay tree structure, and any
   nodes added.  */

static splay_tree splay_tree_new(splay_tree_compare_fn compare_fn, splay_tree_delete_key_fn delete_key_fn, splay_tree_delete_value_fn delete_value_fn)
{
	return (splay_tree_new_with_allocator(compare_fn, delete_key_fn, delete_value_fn, splay_tree_xmalloc_allocate, splay_tree_xmalloc_deallocate, 0));
}

/* Allocate a new splay tree, using COMPARE_FN to compare nodes,
   DELETE_KEY_FN to deallocate keys, and DELETE_VALUE_FN to deallocate
   values.  */

static splay_tree splay_tree_new_with_allocator(splay_tree_compare_fn compare_fn, splay_tree_delete_key_fn delete_key_fn, splay_tree_delete_value_fn delete_value_fn, splay_tree_allocate_fn allocate_fn,
						splay_tree_deallocate_fn deallocate_fn, void *allocate_data)
{
	return splay_tree_new_typed_alloc(compare_fn, delete_key_fn, delete_value_fn, allocate_fn, allocate_fn, deallocate_fn, allocate_data);
}

/*

@deftypefn Supplemental splay_tree splay_tree_new_with_typed_alloc @
(splay_tree_compare_fn @var{compare_fn}, @
splay_tree_delete_key_fn @var{delete_key_fn}, @
splay_tree_delete_value_fn @var{delete_value_fn}, @
splay_tree_allocate_fn @var{tree_allocate_fn}, @
splay_tree_allocate_fn @var{node_allocate_fn}, @
splay_tree_deallocate_fn @var{deallocate_fn}, @
void * @var{allocate_data})

This function creates a splay tree that uses two different allocators
@var{tree_allocate_fn} and @var{node_allocate_fn} to use for allocating the
tree itself and its nodes respectively.  This is useful when variables of
different types need to be allocated with different allocators.

The splay tree will use @var{compare_fn} to compare nodes,
@var{delete_key_fn} to deallocate keys, and @var{delete_value_fn} to
deallocate values.  Keys and values will be deallocated when the
tree is deleted using splay_tree_delete or when a node is removed
using splay_tree_remove.  splay_tree_insert will release the previously
inserted key and value using @var{delete_key_fn} and @var{delete_value_fn}
if the inserted key is already found in the tree.

@end deftypefn

*/

static splay_tree splay_tree_new_typed_alloc(splay_tree_compare_fn compare_fn, splay_tree_delete_key_fn delete_key_fn, splay_tree_delete_value_fn delete_value_fn, splay_tree_allocate_fn tree_allocate_fn,
					     splay_tree_allocate_fn node_allocate_fn, splay_tree_deallocate_fn deallocate_fn, void *allocate_data)
{
	splay_tree sp = (splay_tree) (*tree_allocate_fn)
	    (sizeof(struct splay_tree_s), allocate_data);

	sp->root = 0;
	sp->comp = compare_fn;
	sp->delete_key = delete_key_fn;
	sp->delete_value = delete_value_fn;
	sp->allocate = node_allocate_fn;
	sp->deallocate = deallocate_fn;
	sp->allocate_data = allocate_data;

	return sp;
}

/* Deallocate SP.  */

static void CN(splay_tree_delete) (splay_tree sp) {
	splay_tree_delete_helper(sp, sp->root);
	(*sp->deallocate) ((char *)sp, sp->allocate_data);
	return;
}

/* Insert a new node (associating KEY with DATA) into SP.  If a
   previous node with the indicated KEY exists, its data is replaced
   with the new value.  Returns the new node.  */

static splay_tree_node CN(splay_tree_insert) (splay_tree sp, splay_tree_key key, splay_tree_value value) {
	int comparison = 0;
	splay_tree_node node = (splay_tree_node) 0;

	splay_tree_splay(sp, key);

	if (sp->root) {
		comparison = (*sp->comp) (sp->root->key, key);
	}

	if (sp->root && (comparison == 0)) {
		/* If the root of the tree already has the indicated KEY, delete
		   the old key and old value, and replace them with KEY and  VALUE.  */
		if (sp->delete_key) {
			(*sp->delete_key) (sp->root->key);
		}
		if (sp->delete_value) {
			(*sp->delete_value) (sp->root->value);
		}
		sp->root->key = key;
		sp->root->value = value;
	} else {
		/* Create a new node, and insert it at the root.  */

		node = ((splay_tree_node)
			(*sp->allocate) (sizeof(struct splay_tree_node_s), sp->allocate_data));
		node->key = key;
		node->value = value;

		if (!sp->root) {
			node->left = (splay_tree_node) 0;
			node->right = (splay_tree_node) 0;
		} else if (comparison < 0) {
			node->left = sp->root;
			node->right = node->left->right;
			node->left->right = (splay_tree_node) 0;
		} else {
			node->right = sp->root;
			node->left = node->right->left;
			node->right->left = (splay_tree_node) 0;
		}

		sp->root = node;
	}

	return sp->root;
}

/* Remove KEY from SP.  It is not an error if it did not exist.  */

static void CN(splay_tree_remove) (splay_tree sp, splay_tree_key key) {
	splay_tree_node left = (splay_tree_node) 0;
	splay_tree_node right = (splay_tree_node) 0;

	splay_tree_splay(sp, key);

	if (sp->root && ((*sp->comp) (sp->root->key, key) == 0)) {

		left = sp->root->left;
		right = sp->root->right;

		/* Delete the root node itself.  */
		if (sp->delete_key) {
			(*sp->delete_key) (sp->root->key);
		}
		if (sp->delete_value) {
			(*sp->delete_value) (sp->root->value);
		}
		(*sp->deallocate) (sp->root, sp->allocate_data);

		/* One of the children is now the root.  Doesn't matter much
		   which, so long as we preserve the properties of the tree.  */
		if (left) {
			sp->root = left;

			/* If there was a right child as well, hang it off the 
			   right-most leaf of the left child.  */
			if (right) {
				while (left->right) {
					left = left->right;
				}
				left->right = right;
			}
		} else
			sp->root = right;
	}
	return;
}

/* Lookup KEY in SP, returning VALUE if present, and NULL 
   otherwise.  */

static splay_tree_node splay_tree_lookup(splay_tree sp, splay_tree_key key)
{
	splay_tree_splay(sp, key);

	if (sp->root && ((*sp->comp) (sp->root->key, key) == 0)) {
		return sp->root;
	} else {
		return (splay_tree_node) 0;
	}
}

/* Return the node in SP with the greatest key.  */

static splay_tree_node splay_tree_max(splay_tree sp)
{
	splay_tree_node n = sp->root;

	if (!n) {
		return (splay_tree_node) 0;
	}
	while (n->right) {
		n = n->right;
	}
	return n;
}

/* Return the node in SP with the smallest key.  */

static splay_tree_node CN(splay_tree_min) (splay_tree sp) {
	splay_tree_node n = (splay_tree_node) 0;

	if ((splay_tree) 0 == sp) {
		return (splay_tree_node) 0;
	}

	n = sp->root;

	if ((splay_tree_node) 0 == n) {
		return (splay_tree_node) 0;
	}

	/* now follow left */
	while (n->left) {
		n = n->left;
	}
	return n;
}

/* Return the immediate predecessor KEY, or NULL if there is no
   predecessor.  KEY need not be present in the tree.  */

static splay_tree_node CN(splay_tree_predecessor) (splay_tree sp, splay_tree_key key) {
	int comparison = 0;
	splay_tree_node node = (splay_tree_node) 0;
	/* no tree, no crash. */
	if ((splay_tree) 0 == sp) {
		return (splay_tree_node) 0;
	}

	/* If the tree is empty, there is certainly no predecessor.  */
	if (sp->root == (splay_tree_node) 0) {
		return (splay_tree_node) 0;
	}
	/* Splay the tree around KEY.  That will leave either the KEY
	   itself, its predecessor, or its successor at the root.  */
	splay_tree_splay(sp, key);
	comparison = (*sp->comp) (sp->root->key, key);
	/* If the predecessor is at the root, just return it.  */
	if (comparison < 0) {
		return sp->root;
	}
	/* Otherwise, find the rightmost element of the left subtree.  */
	node = sp->root->left;
	if (node) {
		while (node->right) {
			node = node->right;
		}
	}
	return node;
}

/* Return the immediate successor KEY, or NULL if there is no
   successor.  KEY need not be present in the tree.  */

static splay_tree_node splay_tree_successor(splay_tree sp, splay_tree_key key)
{
	int comparison = 0;
	splay_tree_node node = (splay_tree_node) 0;
	/* If the tree is empty, there is certainly no successor.  */
	if (!sp->root) {
		return (splay_tree_node) 0;
	}
	/* Splay the tree around KEY.  That will leave either the KEY
	   itself, its predecessor, or its successor at the root.  */
	splay_tree_splay(sp, key);
	comparison = (*sp->comp) (sp->root->key, key);
	/* If the successor is at the root, just return it.  */
	if (comparison > 0) {
		return sp->root;
	}

	/* Otherwise, find the leftmost element of the right subtree.  */
	node = sp->root->right;
	if (node) {
		while (node->left) {
			node = node->left;
		}
	}
	return node;
}

/* Call FN, passing it the DATA, for every node in SP, following an
   in-order traversal.  If FN every returns a non-zero value, the
   iteration ceases immediately, and the value is returned.
   Otherwise, this function returns 0.  */

/* slower other way does not use realloc() */
static int splay_tree_foreach(splay_tree sp, splay_tree_foreach_fn fn, void *data)
{
	splay_tree_node spn = (splay_tree_node) 0;
	splay_tree_key key = (splay_tree_key) 0;
	int val = 0;
	if ((splay_tree) 0 == sp) {
		/* no data */
		return (0);
	}
	if (!sp->root) {
		/* no data */
		return (0);
	}
	/* this is slow but will run with bigger splay tree */
	val = 0;
	spn = CN(splay_tree_min) (sp);
	while (spn) {
		key = (splay_tree_key) spn->key;
		val = (*fn) (spn, data);
		/* stop if callback routine returns <> 0 */
		if (val) {
			break;
		}
		spn = splay_tree_successor(sp, key);
	}
	return (val);
}

/* Splay-tree comparison function, treating the keys as ints.  */

static int splay_tree_compare_ints(splay_tree_key k1, splay_tree_key k2)
{
	if ((int)k1 < (int)k2) {
		return -1;
	} else if ((int)k1 > (int)k2) {
		return 1;
	} else {
		return 0;
	}
}

/* Splay-tree comparison function, treating the keys as pointers.  */

static int splay_tree_compare_pointers(splay_tree_key k1, splay_tree_key k2)
{
	if ((char *)k1 < (char *)k2) {
		return -1;
	} else if ((char *)k1 > (char *)k2) {
		return 1;
	} else {
		return 0;
	}
}

/* Splay-tree comparison function, treating the keys as strings. strcmp()  */

static int splay_tree_compare_strings(splay_tree_key k1, splay_tree_key k2)
{
	char *s1 = (char *)k1;
	char *s2 = (char *)k2;
	while (*s1 && (*s1 == *s2)) {
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}

/* Splay-tree delete function, simply using free.  */

static void splay_tree_delete_pointers(splay_tree_value value)
{
	if (value) {
		d4d__main->d4d__free((void *)value);
	}
	return;
}

/* end. */
