
/*
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#ifndef CI_TAB_H
#define CI_TAB_H 1

#define CI_GRAPH 256		/* "graph" */
#define CI_NODE 257		/* "node" */
#define CI_EDGE 258		/* "edge" */
#define CI_COLON 259		/* ":" */
#define CI_BO 260		/* "{" */
#define CI_BC 261		/* "}" */
#define CI_TITLE 262		/* "title" */
#define CI_STRING 263		/* "string" */
#define CI_LABEL 264		/* "label" */
#define CI_SHAPE 265		/* "shape" */
#define CI_ELLIPSE 266		/* "ellipse" */
#define CI_SOURCENAME 267	/* "sourcename" */
#define CI_TARGETNAME 268	/* "targetname" */

#endif

/* end */
