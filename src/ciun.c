
/*
 *  Copyright 2021
 *  This includes splay Copyright (C) 1998-2021 Free Software Foundation, Inc.
 *  Junio H Hamano gitster@pobox.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 *
 * SPDX-License-Identifier: GPL-3.0+
 * License-Filename: LICENSE
 */

#include "config.h"

#include <stdio.h>
#include <zlib.h>

#include "splay-tree.h"
#include "main.h"
#include "ci.h"
#include "ciun.h"

/* by uniq number of node */
static splay_tree ciuniqnode_splaytree = NULL;

void clear_ciuniqnode(void)
{
   ciuniqnode_splaytree = splay_tree_delete(ciuniqnode_splaytree);
   return;
}

void ciuniqnode_add(struct cin *node)
{
   splay_tree_node spn;
   if (node == NULL) {
      /* shouldnothappen */
      return;
   }
   if (ciuniqnode_splaytree == NULL) {
      ciuniqnode_splaytree = splay_tree_new(splay_tree_compare_strings, NULL, NULL);
   }
   spn = splay_tree_lookup(ciuniqnode_splaytree, (splay_tree_key) node->name);
   if (spn) {
      /* shouldnothappen */
      printf("%s(): node `%s' does already exist\n", __func__, node->name);
      fflush(stdout);
      return;
   } else {
      splay_tree_insert(ciuniqnode_splaytree, (splay_tree_key) node->name, (splay_tree_value) node);
   }
   return;
}

struct cin *ciuniqnode(char *name)
{
   splay_tree_node spn;
   if (ciuniqnode_splaytree == NULL) {
      return (NULL);
   }
   spn = splay_tree_lookup(ciuniqnode_splaytree, (splay_tree_key) name);
   if (spn) {
      return ((struct cin *)spn->value);
   } else {
      return (NULL);
   }
}

/* end */
